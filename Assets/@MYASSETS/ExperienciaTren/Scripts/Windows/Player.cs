using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using static UnityEngine.GraphicsBuffer;
using UnityEngine.SceneManagement;

public class PlayerWindows : MonoBehaviour
{
    private Rigidbody rb;
    private float speed = 15f;
    private Vector3 camRotation;
    private Transform cam;

    [Range(-45, -15)]
    public int minAngle = -30;
    [Range(30, 80)]
    public int maxAngle = 45;
    [Range(50, 500)]
    public int sensitivity = 200;

    public bool playerDentro;

    public bool poderMoverse;

    [SerializeField] private GameObject player;

    [SerializeField] private GameObject tren;

    [SerializeField] private GameObject spawn;

    [SerializeField] private WaypointMoverWindows waypointMover;

    private Vector3 coordenadasTren;

    private void Awake()
    {
        cam = Camera.main.transform;
    }

    private void Start()
    {
        playerDentro = false;
        rb = GetComponent<Rigidbody>();
        poderMoverse = true;
        coordenadasTren = spawn.transform.position;
    }

    void Update()
    {
        Move();
        Rotate();
    }
    private void Rotate()
    {

        transform.Rotate(Vector3.up * sensitivity * Time.deltaTime * Input.GetAxis("Mouse X"));

        camRotation.x -= Input.GetAxis("Mouse Y") * sensitivity * Time.deltaTime;
        camRotation.x = Mathf.Clamp(camRotation.x, minAngle, maxAngle);

        cam.localEulerAngles = camRotation;
    }

    private void Move()
    {
        if (poderMoverse == true)
        {
            float hor = Input.GetAxisRaw("Horizontal");
            float ver = Input.GetAxisRaw("Vertical");

            if (hor != 0.0f || ver != 0.0f)
            {
                Vector3 dir = transform.forward * ver + transform.right * hor;

                rb.MovePosition(transform.position + dir * speed * Time.deltaTime);
            }
        }
    }

    public void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Parada"))
        {
            Debug.Log("hi");
            player.transform.localPosition = coordenadasTren;
            player.transform.SetParent(tren.transform);
            poderMoverse = false;
            playerDentro = true;

            if (waypointMover != null)
            {
                waypointMover.ActivarMovimientoTren();
            }
        }
    }
}