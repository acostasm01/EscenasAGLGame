using UnityEngine;

public class PersonController5 : NPCController5
{
    // Start is called before the first frame update
    public override void Start()
    {
        base.Start();
    }

    // Update is called once per frame
    void Update()
    {
        CharacterState = State.Walking;

        if (agent.remainingDistance < 1)
        {
            agent.SetDestination(goals.transform.GetChild(
            Random.Range(0, goals.transform.childCount)).position);
        }
    }
}
