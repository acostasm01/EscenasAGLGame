using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaypointMoverWindows : MonoBehaviour
{
    [SerializeField] private WaypointsWindows waypoints;
    [SerializeField] private float moveSpeed = 22.5f;
    [SerializeField] private float rotationSpeed = 1f; // Velocidad de rotación

    [SerializeField] private float distanceThreshold = 0.1f;

    [SerializeField] private PlayerWindows player;

    [SerializeField] private GameObject spawn;

    [SerializeField] private GameObject tren;

    public Transform currentWaypoint;

    private Vector3 coordenadasParada;

    private Vector3 coordenadasTren;
    private Quaternion rotacionTren;


    void Start()
    {
        currentWaypoint = waypoints.GetNextWayPoint(currentWaypoint);
        transform.position = currentWaypoint.position;

        currentWaypoint = waypoints.GetNextWayPoint(currentWaypoint);
        transform.LookAt(currentWaypoint);

        coordenadasParada = spawn.transform.position;

        coordenadasTren = tren.transform.position;
        rotacionTren = tren.transform.rotation;
    }

    void Update()
    {
        if (player.playerDentro == true)
        {
            transform.position = Vector3.MoveTowards(transform.position, currentWaypoint.position, moveSpeed * Time.deltaTime);
            Quaternion targetRotation = Quaternion.LookRotation(currentWaypoint.position - transform.position);
            transform.rotation = Quaternion.Slerp(transform.rotation, targetRotation, Time.deltaTime * rotationSpeed);

            if (Vector3.Distance(transform.position, currentWaypoint.position) < distanceThreshold)
            {
                if (currentWaypoint.CompareTag("waypointBajada"))
                {
                    moveSpeed = 35f;
                }
                else if (currentWaypoint.CompareTag("waypointSubida"))
                {
                    moveSpeed = 15f;
                }
                else if (currentWaypoint.CompareTag("waypointPrePreFinal"))
                {
                    moveSpeed = 18.5f;
                }
                else if (currentWaypoint.CompareTag("waypointPreFinal"))
                {
                    moveSpeed = 10f;
                }
                else if (currentWaypoint.CompareTag("waypointFinal"))
                {
                    moveSpeed = 0f;

                    player.transform.SetParent(null);
                    player.transform.localPosition = coordenadasParada;
                    player.playerDentro = false;
                    player.poderMoverse = true;
                    // Restablecer la posición y orientación del tren
                    tren.transform.position = coordenadasTren;
                    tren.transform.rotation = rotacionTren;
                }
                else
                {
                    moveSpeed = 22.5f;
                }
                currentWaypoint = waypoints.GetNextWayPoint(currentWaypoint);
            }
        }
    }

    public void ActivarMovimientoTren()
    {
        moveSpeed = 22.5f;
    }

    public void ResetearMovimientoTren()
    {
        // Reiniciar velocidad de movimiento
        moveSpeed = 22.5f;
        // Colocar al tren en el primer waypoint
        currentWaypoint = waypoints.GetNextWayPoint(null);
        // transform.position = currentWaypoint.position;
        // transform.LookAt(currentWaypoint);
    }
}