using UnityEngine;
using UnityEngine.AI;

public abstract class NPCController3 : MonoBehaviour
{
    protected NavMeshAgent agent;
    protected GameObject goals;

    public enum State
    {
        SafePlace,
        Walking,
        Fleeing
    }

    public State CharacterState { get; set; }

    public virtual void Start()
    {
        agent = GetComponent<NavMeshAgent>();
        goals = GameObject.FindGameObjectWithTag("goals3");

        agent.SetDestination(
            goals.transform.GetChild(
                Random.Range(0, goals.transform.childCount)).position);

        CharacterState = State.Walking;
    }
}
