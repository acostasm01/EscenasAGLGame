using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EscenaTrenGameManagerWindows : MonoBehaviour
{
    [SerializeField] public bool juegoPausado = false;
    [SerializeField] private PlayerWindows player;
    [SerializeField] private GameObject tren;
    [SerializeField] private WaypointMoverWindows WaypointMover;
    [SerializeField] private GameObject playerP;
    [SerializeField] private GameObject canvasPausa;
    [SerializeField] private GameObject spawn;

    private Vector3 coordenadasTren;
    private Quaternion rotacionTren;
    private Vector3 coordenadasPlayer;
    private Vector3 coordenadasParada;
    private GameStateManager manager;

    void Start()
    {
        canvasPausa.gameObject.SetActive(false);
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
        coordenadasTren = tren.transform.position;
        rotacionTren = tren.transform.rotation;
        coordenadasPlayer = playerP.transform.position;
        coordenadasParada = spawn.transform.position;
        manager = GameStateManager.instance;
    }

    void Update()
    {
        Pause();
    }

    public void Pause()
    {

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            juegoPausado = !juegoPausado;

            if (juegoPausado)
            {
                PausarJuego();

            }
            else
            {
                ReanudarJuego();

            }
        }

    }

    public void PausarJuego()
    {

        Time.timeScale = 0f;
        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.None;
        canvasPausa.gameObject.SetActive(true);
    }


    public void ReanudarJuego()
    {
        canvasPausa.gameObject.SetActive(false);
        Time.timeScale = 1f;
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
    }

    public void Volver()
    {
        // Restablecer la posición y orientación del tren
        tren.transform.position = coordenadasTren;
        tren.transform.rotation = rotacionTren;

        // Restablecer la posición del jugador
        player.transform.SetParent(null);
        player.transform.localPosition = coordenadasParada;

        // Restablecer el estado del jugador
        player.playerDentro = false;
        player.poderMoverse = true;

        // Restablecer el movimiento del tren
        WaypointMover.ResetearMovimientoTren();

        // Reanudar el juego si estaba pausado
        if (juegoPausado)
        {
            ReanudarJuego();
            juegoPausado = !juegoPausado;
        }
    }
    public void Salir()
    {
        manager.QuitToMainScene();
    }
}
