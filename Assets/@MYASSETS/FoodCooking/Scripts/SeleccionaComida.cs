using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;


public class SeleccionaComida : MonoBehaviour
{
    private ControladorJuego manager;
    public Button botonHamburgesa;
    public Button botonPatatas;
    public Button botonPerrito;
    public Button botonPizza;
    public GameObject comidaHambueguesa;
    public GameObject comidaPatatas;
    public GameObject comidaPerrito;
    public GameObject comidaPizza;
    private GameObject comidaSeleccionada;
    public string comidaElegida;
    private List<Button> listaBotonesComida;
    bool devolverComida = false;
    Vector3 posicionInicial;
    

    void Start()
    {
        manager = ControladorJuego.instance;
        manager.setSeleccionaComida(null);
    }

    public void ComidaElegida(GameObject comida)
    {
        if (comidaSeleccionada != null)
        {
        // Si ya hay una comida seleccionada, devu�lvela a su posici�n inicial
        DevolverComida(comidaSeleccionada);
        }

        // Almacena la nueva comida seleccionada
        comidaSeleccionada = comida;
        posicionInicial = comida.transform.position;
        comidaElegida = comida.tag.ToString();

        
        listaBotonesComida = manager.GetBotonesComida();
        // Deshabilitar todos los botones de comida y cambia la posicion de la comida seleccionada a la bandeja
        foreach (Button button in listaBotonesComida)
        {
            if (!button.CompareTag(comidaElegida))
            {
                DeshabilitarBoton(button);
            }
            else
            {
                button.transform.position = new Vector3(53f, -7f, 0);
                manager.setSeleccionaComida(comidaSeleccionada);
                Devolver();
            }
        }
    }

    public void quitarComida(GameObject item)
    {
        if (!devolverComida)
        {
            foreach (Button button in listaBotonesComida)
            {
                HabilitarBoton(button);
            }

            // Devolver la comida seleccionada a su posici�n inicial
            DevolverComida(comidaSeleccionada);
        }
    }

    private void DevolverComida(GameObject comida)
    {
        if (comida != null)
        {
            comida.transform.position = posicionInicial;
            foreach (Button button in listaBotonesComida)
            {
                HabilitarBoton(button);
            }
            manager.setSeleccionaComida(null);
        }
    }

    private void DeshabilitarBoton(Button boton)
    {
        // Deshabilitar el bot�n de esta comida
        boton.interactable = false;
    }

    private void HabilitarBoton(Button button)
    {
        button.interactable = true;
    }

    private void Devolver()
    {
        if (devolverComida)
        {
            devolverComida = false;
            
        }
        else
        {
            devolverComida = true;
            
        }
    }

}
//53f, -7f, 0f posicion bandeja 

