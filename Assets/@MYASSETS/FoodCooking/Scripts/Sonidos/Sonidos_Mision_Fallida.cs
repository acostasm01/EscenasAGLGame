using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class Sonidos_Mision_Fallida : MonoBehaviour
{
    public AudioSource source { get { return GetComponent<AudioSource>(); } }
    public Button btn_Fallido { get { return GetComponent<Button>(); } }
    public AudioClip clip;

    // Start is called before the first frame update
    void Start()
    {
        gameObject.AddComponent<AudioSource>();
        btn_Fallido.onClick.AddListener(Sonido_Fallida);
    }

    void Sonido_Fallida()
    {
        source.PlayOneShot(clip);
    }
}
