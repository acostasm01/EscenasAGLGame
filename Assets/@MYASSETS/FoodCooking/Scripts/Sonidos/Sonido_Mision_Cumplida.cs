using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class Sonido_Mision_Cumplida : MonoBehaviour
{
    public AudioSource source { get { return GetComponent<AudioSource>(); } }
    public Button btn_Cumplido { get { return GetComponent<Button>(); } }
    public AudioClip clip;

    // Start is called before the first frame update
    void Start()
    {
        gameObject.AddComponent<AudioSource>();
        btn_Cumplido.onClick.AddListener(Sonido_Cumplida);
    }

    void Sonido_Cumplida()
    {
        source.PlayOneShot(clip);
    }
}
