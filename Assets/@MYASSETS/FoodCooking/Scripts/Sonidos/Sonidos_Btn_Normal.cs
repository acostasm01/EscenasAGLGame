using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Sonidos_Btn_Normal : MonoBehaviour
{
    public AudioSource source { get { return GetComponent<AudioSource>(); } }
    public Button btn_normal {  get { return GetComponent<Button>(); } }
    public AudioClip clip;
    
    // Start is called before the first frame update
    void Start()
    {
        gameObject.AddComponent<AudioSource>();
        btn_normal.onClick.AddListener(Sonido_Normal);
    }

    void Sonido_Normal()
    {
        source.PlayOneShot(clip);
    }

    
}
