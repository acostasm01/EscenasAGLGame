using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.UI;

public class SonidoSettings : MonoBehaviour
{
    public Slider slidervol;
    public float sliderValue;
    public Image imageMute;

    private void Start()
    {
        float v = PlayerPrefs.GetFloat("volumenAudio", 0.5f);
        slidervol.value = v;
        AudioListener.volume = slidervol.value;
        RevisarSiEstoyMuter();
    }
    public void ChangeVolume(float valor)
    {
        sliderValue = valor;
        PlayerPrefs.SetFloat("volumenAudio", sliderValue);
        AudioListener.volume = slidervol.value;
        RevisarSiEstoyMuter();
    }

    public void RevisarSiEstoyMuter()
    {
        if (sliderValue == 0)
        {
            imageMute.enabled = true;

        }
        else
        {
            imageMute.enabled= false;
        }
    }

}
