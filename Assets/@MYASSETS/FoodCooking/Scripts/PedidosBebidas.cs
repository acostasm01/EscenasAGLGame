using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PedidosBebidas : MonoBehaviour
{
    public GameObject[] prefabsBebidas;
    private GameObject bebidaPedido;
    public GameObject bebidaCorrecta;
    private ControladorJuego manager;
    private GameObject bebidaSeleccionada;

    void Start()
    {
        manager = ControladorJuego.instance;
        MostrarPedidoBebidaAleatorio();
    }
   
    public void MostrarPedidoBebidaAleatorio()
    {
        // Solo genera una bebida ya que se generaba dos veces
        if (bebidaPedido != null)
        {
            Destroy(bebidaPedido);
        }

        if (prefabsBebidas != null && prefabsBebidas.Length > 0)
        {
            // Bebida aleatoria
            int indiceAleatorio = Random.Range(0, prefabsBebidas.Length);

            // Instancia la bebida y col�cala en la posici�n del GameObject actual
            bebidaPedido = Instantiate(prefabsBebidas[indiceAleatorio], transform.position, Quaternion.identity, transform);

            bebidaCorrecta = prefabsBebidas[indiceAleatorio];

            manager.setBebidaCorrecta(bebidaPedido);
        }  

    }
    
}
