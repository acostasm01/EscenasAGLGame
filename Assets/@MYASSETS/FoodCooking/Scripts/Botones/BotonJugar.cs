using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BotonJugar : MonoBehaviour
{
    public Button boton;
    public float tiempoDesactivado = 5f;

    private bool botonDeshabilitado = false;

    void Start()
    {
        // Agregar un listener para el evento de clic del bot�n
        boton.onClick.AddListener(OnClickBoton);
    }

    private void OnClickBoton()
    {
        if (!botonDeshabilitado)
        {
            // Desactivar el bot�n
            boton.interactable = false;

            // Iniciar una corutina para reactivar el bot�n despu�s de un tiempo
            StartCoroutine(EsperarYActivarBoton());
        }
    }

    private IEnumerator EsperarYActivarBoton()
    {
        yield return new WaitForSeconds(tiempoDesactivado);

        // Activar el bot�n nuevamente
        boton.interactable = true;
        botonDeshabilitado = false;
    }
}
