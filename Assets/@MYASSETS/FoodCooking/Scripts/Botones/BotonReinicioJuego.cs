using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BotonReinicioJuego : MonoBehaviour
{
    public ControladorJuego controladorJuego; 
    void Start()
    {
        // Obt�n la referencia al script ControladorJuego si no se asign�
        if (controladorJuego == null)
        {
            controladorJuego = FindObjectOfType<ControladorJuego>();
        }

        // Asigna la funci�n ReiniciarPedido al evento de clic del bot�n
        GetComponent<Button>().onClick.AddListener(ReiniciarPedidoCorrecto);
    }

    void ReiniciarPedidoCorrecto()
    {
        // Llama a la funci�n ReiniciarPedido en el script ControladorJuego
        controladorJuego.ReiniciarPedido();
    }

}
