using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Boton_Controles : MonoBehaviour
{
    public GameObject controles;
    public GameObject btn_entendido;
    public GameObject btn_seguir;
    public GameObject pedidocomida;
    public GameObject pedidobebida;
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void DeshabilitarControles()
    {
        controles.SetActive(false);
        btn_entendido.SetActive(false);
        btn_seguir.SetActive(true);
        pedidocomida.SetActive(true);
        pedidobebida.SetActive(true);
    }
}
