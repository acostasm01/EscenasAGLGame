using UnityEditor;
using UnityEngine;

public class PedidosComida : MonoBehaviour
{
    public GameObject[] prefabsComida;
    private GameObject comidaPedido;
    public GameObject comidaCorrecta;
    private GameObject comidaSeleccionada;
    private ControladorJuego manager;

   
    void Start()
    {
        manager = ControladorJuego.instance;
        MostrarPedidoComidaAleatorio();
    }

    public void MostrarPedidoComidaAleatorio()
    {

        // Solo genera una comida ya que se generaba dos veces
        if (comidaPedido != null)
        {
            Destroy(comidaPedido);
        }

        if (prefabsComida != null && prefabsComida.Length > 0)
        {
            // Comida aleatoria
            int indiceAleatorio = Random.Range(0, prefabsComida.Length);

            // Instancia la comida y col�cala en la posici�n del GameObject actual
            comidaPedido = Instantiate(prefabsComida[indiceAleatorio], transform.position, Quaternion.identity, transform);
            
            comidaCorrecta = prefabsComida[indiceAleatorio];
            manager.setComidaCorrecta(comidaPedido);
        }

    } 
}