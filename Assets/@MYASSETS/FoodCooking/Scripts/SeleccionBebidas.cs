using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;


public class SeleccionBebidas : MonoBehaviour
{
    private ControladorJuego manager;
    public Button botonCola;
    public Button botonManzana;
    public Button botonLimon;
    public Button botonNaranja;
    public GameObject bebidaColaPrefab;
    public GameObject bebidaManzanaPrefab;
    public GameObject bebidaLimonPrefab;
    public GameObject bebidaNaranjaPrefab;
    private GameObject bebidaSeleccionada;
    public string bebidaElegida;
    private List<Button> listaBotonesBebida;
    bool devolverBebida = false;
    Vector3 posicionInicial;

    void Start()
    {
        manager = ControladorJuego.instance;
        manager.setSeleccionaBebida(null);

    }

    public void BebidaElegida(GameObject bebida)
    {
        if (bebidaSeleccionada != null)
        {
            // Si ya hay una bebida seleccionada, devu�lvela a su posici�n inicial
            DevolverBebida(bebidaSeleccionada);

        }

        // Almacena la nueva bebida seleccionada
        bebidaSeleccionada = bebida;
        posicionInicial = bebida.transform.position;
        bebidaElegida = bebida.tag.ToString();

        listaBotonesBebida = manager.GetBotonesBebida();
        // Deshabilitar todos los botones de bebida
        foreach (Button button in listaBotonesBebida)
        {
            if (!button.CompareTag(bebidaElegida))
            {
                DeshabilitarBoton(button);
            }
            else
            {
                button.transform.position = new Vector3(116f, -7f, 0f);
                manager.setSeleccionaBebida(bebida);
                Devolver();
            }
        }
    }

    public void quitarBebida(GameObject item)
    {
        if (!devolverBebida)
        {
            foreach (Button button in listaBotonesBebida)
            {
                HabilitarBoton(button);
            }

            // Devolver la comida seleccionada a su posici�n inicial
            DevolverBebida(bebidaSeleccionada);

        }
    }

    private void DevolverBebida(GameObject bebida)
    {
        if (bebida != null)
        {
            bebida.transform.position = posicionInicial;
            foreach (Button button in listaBotonesBebida)
            {
                HabilitarBoton(button);
            }
            manager.setSeleccionaBebida(null);
        }
    }

    private void DeshabilitarBoton(Button boton)
    {
        // Deshabilitar el bot�n de esta comida
        boton.interactable = false;
    }

    private void HabilitarBoton(Button button)
    {
        button.interactable = true;
    }

    private void Devolver()
    {
        if (devolverBebida)
        {
            devolverBebida = false;
            
        }
        else
        {
            devolverBebida = true;
            
        }
    }
}
//116f, -7f, 0f posicion bandeja