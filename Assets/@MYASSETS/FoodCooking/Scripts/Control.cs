
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Control : MonoBehaviour
{

    public Vector3 posicion;
    public Sprite comida;
    private ControladorJuego manager;
    
    private GameStateManager Statemanager;
    
    public Button volver;

    void Start()
    {
        manager = ControladorJuego.instance;
        Statemanager = GameStateManager.instance;
        

        volver.onClick.AddListener(() => btnVolver());
    }


    public void Exit()
    {
        if (manager.juegoCompleto) {
            Statemanager.MarcarEscenaComoCompletada(SceneManager.GetActiveScene().buildIndex); 
        }else { 
            Statemanager.QuitToMainScene();
        }
    }

    
    public void MovimientoCamaraX()
    {
        posicion = transform.position;
    }

    public void btnVolver()
    {
        transform.position = posicion;
    }

    public void CambiarCamara_Jugar()
    {
        
        
        Invoke("Mov_Cam_Pedidos1", 2);
    }

    void Mov_Cam_Pedidos1()
    {
        transform.position = Camera.main.transform.position + new Vector3(59f, 0f, 0f);
    }

    public void Mov_Cam_Comidas()
    {
            transform.position = Camera.main.transform.position + new Vector3(54f, 0f, 0f);
    }
    
    public void Mov_Cam_Bebidas()
    {
        if (manager.getComidaSeleccionada() != null)
        {
            transform.position = Camera.main.transform.position + new Vector3(57f, 0f, 0f);
            GameObject comida = manager.getComidaSeleccionada();
            comida.transform.position = new Vector3(110f, -7f, 0);
            

        }
        else
        {
            return;
        }
    }

    public void Mov_Cam_Bebidas_Pedidos()
    {
        if (manager.getBebidaSeleccionada() != null)
        {
            transform.position = Camera.main.transform.position + new Vector3(59f, 0f, 0f);
            manager.SeleccionarPedido();
            GameObject comida = manager.getComidaSeleccionada();
            comida.transform.position = new Vector3(168f, 1f, 0);
            GameObject bebida = manager.getBebidaSeleccionada();
            bebida.transform.position = new Vector3(175f, 1f, 0);
        }
        else 
        { 
            return;
        }    
    }

    public void Mov_Mision_Cumplida()
    {
        transform.position = Camera.main.transform.position + new Vector3(59f, 0f, 0f);
        manager.juegoCompleto = true;
    }

    public void Mov_Mision_Fallida()
    {
        transform.position = Camera.main.transform.position + new Vector3(125f, 0f, 0f);
    }

    public void Mov_Inicio_Cumplida()
    {
        transform.position = Camera.main.transform.position + new Vector3(-288f, 0f, 0f);
        
    }

    public void Mov_Inicio_Fallida()
    {
        transform.position = Camera.main.transform.position + new Vector3(-354f, 0f, 0f);
    }

    public void Mov_Opciones_Principal()
    {
        MovimientoCamaraX();
        transform.position = Camera.main.transform.position + new Vector3(-54f, 0f, 0f);
    }

    public void Mov_Opciones_Pedidos()
    {
        MovimientoCamaraX();
        transform.position = Camera.main.transform.position + new Vector3(-113f, 0f, 0f);
    }

    public void Mov_Opciones_Comidas()
    {
        MovimientoCamaraX();
        transform.position = Camera.main.transform.position + new Vector3(-167f, 0f, 0f);
    }

    public void Mov_Opciones_Bebidas()
    {
        MovimientoCamaraX();
        transform.position = Camera.main.transform.position + new Vector3(-224f, 0f, 0f);
    }

    public void Mov_Opciones_Pedidos_Final()
    {
        MovimientoCamaraX();
        transform.position = Camera.main.transform.position + new Vector3(-283f, 0f, 0f);
    }

    public void Mov_Retroceso_Bebida_Comida()
    {
        transform.position = Camera.main.transform.position + new Vector3(-57f, 0f, 0f);
    }
  
}
