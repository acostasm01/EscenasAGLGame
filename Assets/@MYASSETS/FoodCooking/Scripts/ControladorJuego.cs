using JetBrains.Annotations;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class ControladorJuego : MonoBehaviour
{
    //Botones y gameObjets de Comidas
    public Button botonHamburgesa;
    public Button botonPatatas;
    public Button botonPerrito;
    public Button botonPizza;
    private List<Button> ListaBotonesComida;

    //Botones y gameObjets de Comidas
    public Button botonCola;
    public Button botonManzana;
    public Button botonLimon;
    public Button botonNaranja;
    private List<Button> ListaBotonesBebidas;

    public GameObject pedidosComida;
    public GameObject pedidosBebida;
    public GameObject seleccionaComida;
    public GameObject seleccionBebida;

    //Seleccion y comprobacion pedido
    public bool SeleccionComida = true;
    public bool SeleccionBebida = false;
    public bool pedidoElegido = false;
    public bool Comprobacion = false;
    public bool Correcta = false;
    public bool Incorrecta = false;

    //(Botones y sprites) Mostrar la comprobacion del pedido seleccionado
    public Button btn_correcto;
    public Button btn_incorrecto;
    public GameObject sprite_correcto;
    public GameObject sprite_incorrecto;

    private PedidosBebidas PedidosBebidas;
    private PedidosComida PedidosComidas;

    public Transform posicionComidaBebidas;
    public Transform posicionComidaPedidos;
    public Transform posicionBebidaPedidos;

    private GameStateManager gameStateManager;
    public bool juegoCompleto { get; set; }

    #region Singleton

    //Use a property so other code can't assign instance.
    static ControladorJuego _instance;
    public static ControladorJuego instance
    {
        get
        {
            if (!_instance)
                _instance = new GameObject("GameManager", typeof(ControladorJuego)).GetComponent<ControladorJuego>();
            return _instance;
        }
    }

    void EnsureSingleton()
    {
        if (!_instance)
            _instance = this;

        var gameManagerInstances = GameObject.FindObjectsOfType<ControladorJuego>();

        if (gameManagerInstances.Length != 1)
        {
            Debug.LogError("Only one instance of the GameManager manager should ever exist. Removing extraneous.");

            foreach (var otherInstance in gameManagerInstances)
            {
                if (otherInstance != instance)
                {

                    if (otherInstance.gameObject == instance.gameObject)
                        Destroy(otherInstance);
                    else
                        Destroy(otherInstance.gameObject);
                }
            }
        }

    }
    #endregion Singleton

    private void Awake()
    {
        EnsureSingleton();
    }
    void Start()
    {
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
        gameStateManager = GameStateManager.instance;
        PedidosBebidas = new PedidosBebidas();
        PedidosComidas = new PedidosComida();


        ListaBotonesComida = new List<Button>
        {
            botonHamburgesa,
            botonPatatas,
            botonPerrito,
            botonPizza
        };

        ListaBotonesBebidas = new List<Button>
        {
            botonCola,
            botonManzana,
            botonLimon,
            botonNaranja
        };


    }

    
    void Update()
    {
        //Comproobacion pedido
        if (Comprobacion)
        {
            VerificarSeleccion();
        }

    }

    public void ReiniciarPedido()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
       
    }
    #region getterysetter
    public List<Button> GetBotonesComida()
    {
        return ListaBotonesComida;
    }
    public List<Button> GetBotonesBebida()
    {
        return ListaBotonesBebidas;
    }
    public void setSeleccionaComida(GameObject comida)
    {
        this.seleccionaComida = comida;
    }
    public GameObject getComidaSeleccionada()
    {
        return seleccionaComida;
    }
    public void setSeleccionaBebida(GameObject bebida)
    {
        this.seleccionBebida = bebida;
    }
    public GameObject getBebidaSeleccionada()
    {
        return seleccionBebida;
    }
    public GameObject getComidaCorrecta()
    {
        return pedidosComida;
    }
    public void setComidaCorrecta(GameObject correcta)
    {
        pedidosComida = correcta;
    }
    public GameObject getBebidaCorrecta()
    {
        return pedidosBebida;
    }
    public void setBebidaCorrecta(GameObject correcta)
    {
        pedidosBebida = correcta;
    }
    #endregion getterysetter

    //-----------PARTE COMPROBACION PEDIDO FINAL-------------------
    public void SeleccionarPedido()
    {
            seleccionaComida = getComidaSeleccionada();
            seleccionBebida = getBebidaSeleccionada();
            pedidoElegido = true;
            Comprobacion = true;
            VerificarSeleccion();
    }

    private void VerificarSeleccion()
    {
        if (seleccionaComida != null && seleccionBebida != null)
        {

            string nombreComida = seleccionaComida.tag;
            string nombreBebida = seleccionBebida.tag;
           
            bool seleccionCorrecta = EsSeleccionCorrecta(nombreComida, nombreBebida);

            if (seleccionCorrecta)
            {
                MostrarBotonesSpritesCorrectos();
            }
            else
            {
                
                MostrarBotonesSpritesIncorrectos();
            }

            // Restablece las variables para el pr�ximo ciclo
            Comprobacion = false;
            pedidoElegido = false;
        }
    }

    private bool EsSeleccionCorrecta(string comida, string bebida)
    {
       if(pedidosComida.CompareTag(comida) && pedidosBebida.CompareTag(bebida))
        {
            return true;
        }
        else
        {
            
            return false;
        }
    }

    private void MostrarBotonesSpritesCorrectos()
    {
        // Mostrar bot�n y sprite correctos, y ocultar los incorrectos
        btn_correcto.gameObject.SetActive(true);
        sprite_correcto.SetActive(true);
        btn_incorrecto.gameObject.SetActive(false);
        sprite_incorrecto.SetActive(false);
    }

    private void MostrarBotonesSpritesIncorrectos()
    {
        // Mostrar bot�n y sprite incorrectos, y ocultar los correctos
        btn_correcto.gameObject.SetActive(false);
        sprite_correcto.SetActive(false);
        btn_incorrecto.gameObject.SetActive(true);
        sprite_incorrecto.SetActive(true);
    }

}
