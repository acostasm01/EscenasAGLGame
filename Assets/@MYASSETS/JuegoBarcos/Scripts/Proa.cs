using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Proa : MonoBehaviour
{
    public GameObject barco;
    Barco scriptBarco;

    private void Start()
    {
        scriptBarco = barco.GetComponent<Barco>();
    }
    // Start is called before the first frame update
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Bala"))
        {
            if (barco != null)
            {
                Destroy(other.gameObject);
                scriptBarco.Hit();
            }
        }
    }
}

