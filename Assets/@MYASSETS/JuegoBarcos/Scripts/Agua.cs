using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Agua : MonoBehaviour
{
    public GameObject BulletPrefab;
    private JuegoBarcoGameManager manager;
    private AudioSource audioSource;
    void Start()
    {
        manager = JuegoBarcoGameManager.instance;
        audioSource = gameObject.GetComponent<AudioSource>();
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Bala")
        {
            Destroy(other.gameObject);
            manager.setIsShooting(false);
            audioSource.Play();
        }
        else return;
    }
}
