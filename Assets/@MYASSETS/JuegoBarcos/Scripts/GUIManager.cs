using System.Collections;
using System.Collections.Generic;
using UnityEngine.EventSystems;
using UnityEngine;
using TMPro;
using Unity.VisualScripting;

public class GUIManager : MonoBehaviour
{
    private static JuegoBarcoGameManager manager;
    public TMP_Text m_TextComponent;
    public Camera cam;
    public Camera camToMove;
    private Vector3 posicionInicCam;
    public RectTransform minimapa;
    RaycastHit hit;
    Ray ray;
    Vector3 movePoint;
    [SerializeField]
    float offset;
    // Start is called before the first frame update
    void Start()
    {
        manager = JuegoBarcoGameManager.instance;
        posicionInicCam = camToMove.transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        m_TextComponent.text = "Puntuacion " + manager.getPuntuacion() + "/15";
        if (manager.isPaused) { return; }
        if (manager.isShooting)
        {
            return;
        }
        if (IspointerOverUiObject())
        {
                if (Input.GetMouseButton(1)){
                camToMove.enabled = true;
                    ray = cam.ScreenPointToRay(Input.mousePosition);
                
                if (Physics.Raycast(ray, out hit, Mathf.Infinity))
                {
                    float normalizedX = Mathf.InverseLerp(3.1f, 9.7f, hit.point.x);
                    float targetX = Mathf.Lerp(0f, 300f, normalizedX);
                    targetX = Mathf.Clamp(targetX, 0f, 300);
                    movePoint = new Vector3(targetX, 1, -10);
                    camToMove.transform.position = movePoint;
                }
            }
            if (Input.GetMouseButtonUp(1))
            {
              
                camToMove.transform.position = posicionInicCam;
                camToMove.enabled = false;
            }
        }
    }
    private bool IspointerOverUiObject()
    {
        PointerEventData EventDataCurrentPosition = new PointerEventData(EventSystem.current);
        EventDataCurrentPosition.position = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
        List<RaycastResult> result = new List<RaycastResult>();
        EventSystem.current.RaycastAll(EventDataCurrentPosition, result);
        return result.Count > 0;
        
    }

}
