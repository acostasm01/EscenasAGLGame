using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;

public class Barco : MonoBehaviour
{
    public GameObject ship;
    public int Health;
    public float velocity;
    public float timePassed= 5f;
    public int timeRemaining;
    public int duration = 5;

    public bool isCountingDown = false;
    private Animator animator;
    public Animator minimapAnimator;
    public bool isDeath
    {
        get { return Health == 0; }
    }
    private static JuegoBarcoGameManager manager;

    private void Start()
    {
            manager = JuegoBarcoGameManager.instance;
            velocity = manager.getVelocity();
            animator = GetComponent<Animator>();
    }

    void Update()
    {
        if (!isCountingDown)
        {
            isCountingDown = true;
            timeRemaining = duration;
            Invoke("_tick", 1f);
        }
        transform.Translate(Vector3.left * velocity * Time.deltaTime);
    }
    public void Hit()
    {
        manager.setIsShooting(false);
        Health--;
        if (isDeath)
        {
            animator.SetTrigger("Muerte");
            Invoke("EliminarGameObject", animator.GetCurrentAnimatorStateInfo(0).length);
            minimapAnimator.SetTrigger("Muerte");
            manager.barcoHundido();
            
        }
    }

    private void _tick() {
         timeRemaining--;
         if(timeRemaining > 0) {

             Invoke ( "_tick", 1f );
             velocity += 0.4f;
        } else {
             isCountingDown = false;
         }
    }
    void EliminarGameObject()
    {
        Destroy(gameObject);
    }
}
