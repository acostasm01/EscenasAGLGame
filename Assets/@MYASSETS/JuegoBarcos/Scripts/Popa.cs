using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Popa : MonoBehaviour
{
    public GameObject barco;
    Barco scriptBarco;
    // Start is called before the first frame update
    private void Start()
    {
        scriptBarco = barco.GetComponent<Barco>();
    }
    private void OnTriggerEnter(Collider other)
    {
       if (other.CompareTag("Bala"))
        {
            if(barco != null)
            {
            Destroy(other.gameObject);
            scriptBarco.Hit();
            }
            
        }
    }
    
}
