using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.SceneManagement;

public class JuegoBarcoGameManager : MonoBehaviour
{
    public GameObject barco;
    public int timeRemaining;
    public int puntuacion;
    public float velocity;
    public int duration = 15;
    public bool isCountingDown = false;
    public bool isPaused = true;
    public CannonManager cannonManager;
    public GameObject menuDeath;
    public MenuBarcos menu;
    public bool isShooting;
    public bool isPlaying;
    GameStateManager manager;

    // Start is called before the first frame update
    #region Singleton

    //Use a property so other code can't assign instance.
    static JuegoBarcoGameManager _instance;
    public static JuegoBarcoGameManager instance
    {
        get
        {
            if (!_instance)
                _instance = new GameObject("GameManager", typeof(JuegoBarcoGameManager)).GetComponent<JuegoBarcoGameManager>();
            return _instance;
        }
    }

    void EnsureSingleton()
    {
        if (!_instance)
            _instance = this;

        var gameManagerInstances = GameObject.FindObjectsOfType<JuegoBarcoGameManager>();

        if (gameManagerInstances.Length != 1)
        {
            Debug.LogError("Only one instance of the GameManager manager should ever exist. Removing extraneous.");

            foreach (var otherInstance in gameManagerInstances)
            {
                if (otherInstance != instance)
                {

                    if (otherInstance.gameObject == instance.gameObject)
                        Destroy(otherInstance);
                    else
                        Destroy(otherInstance.gameObject);
                }
            }
        }

    }
    #endregion Singleton

    
    
    void Awake()
    {
        EnsureSingleton();
    }
    private void Start()
    {
        Screen.SetResolution(1920, 1080, FullScreenMode.FullScreenWindow);
        manager = GameStateManager.instance;
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
    }
    // Update is called once per frame
    void Update()
    {
        TogglePause();
        if (!isCountingDown)
        {
            isCountingDown = true;
            timeRemaining = duration;
            Invoke("_tick", 1f);
        }
        if(puntuacion == 15)
        {
            manager.MarcarEscenaComoCompletada(SceneManager.GetActiveScene().buildIndex);
        }
        if(Input.GetKeyDown(KeyCode.Escape) && isPlaying)
        {
            menu.Pause();
        }
        
    }
    private void _tick()
        {
            timeRemaining--;
            if (timeRemaining > 0)
            {
                Invoke("_tick", 1f);
            }
            else
            {
            velocity += 1.6f;
            Instantiate(barco);
            
                isCountingDown = false;
            }
        }
    public void barcoHundido()
    {
        puntuacion++;
    }
    public float getVelocity()
    {
        return this.velocity;
    }

    public void Death()
    {
        menuDeath.SetActive(true);
    }
    public void TogglePause()
    {
        if (isPaused)
        {
            Time.timeScale = 0f;
            cannonManager.AllowToShoot = false;
        }
        else
        {
            Time.timeScale = 1f;
            cannonManager.AllowToShoot = true;
        }
    }
    public bool getIsPaused()
    {
        return isPaused;
    }
    public void setIsPaused(bool isPaused)
    {
        this.isPaused = isPaused;
    }
    public int getPuntuacion()
    {
        return puntuacion;
    }
    public bool getisShooting()
    {
        return isShooting;
    }
    public void setIsShooting(bool isShooting)
    {
        this.isShooting = isShooting;
    }
    public bool getisPlaying()
    {
        return isPlaying;
    }
    public void setisPlaying(bool isPlaying)
    {
        this.isPlaying = isPlaying;
    }
}
