using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.UI;

public class MenuBarcos : MonoBehaviour
{
    public GameObject MenuInicio;
    public GameObject MenuPause;
    JuegoBarcoGameManager gameManager;
    GameStateManager manager;
    private void Start()
    {
        gameManager = JuegoBarcoGameManager.instance;
        manager = GameStateManager.instance;
    }

    public void Play()
    {
        gameObject.SetActive(false);
        MenuInicio.SetActive(false);
        gameManager.setIsPaused(false);
        gameManager.setisPlaying(true);
    }

    public void Pause()
    {
        gameObject.SetActive(true);
        MenuPause.SetActive(true);
        gameManager.setIsPaused(true);
    }
    public void Resume()
    {
        gameObject.SetActive(false);
        MenuPause.SetActive(false);
        gameManager.setIsPaused(false);
    }
    public void Exit()
    {
        manager.QuitToMainScene();
    }

}
