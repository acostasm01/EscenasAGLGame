using Unity.VisualScripting;
using UnityEngine;

public class CannonManager : MonoBehaviour
{
    public GameObject cannonBallPrefab;
    public Transform firePoint;
    public LineRenderer lineRenderer;
    public LineRenderer minimaplineRenderer;
    private GameObject currentBullet;
    private const int N_TRAJECTORY_POINTS = 10;
    Vector3 mousePos;
    private Camera _cam;
    private bool _pressingMouse = false;
    private Vector3 _initialVelocity;
    Vector3 force;
    private static JuegoBarcoGameManager manager;
    public bool AllowToShoot = false;
    Ray ray;
    private AudioSource audioSource;


    void Start()
    {

        _cam = Camera.main;
        manager = JuegoBarcoGameManager.instance;
        lineRenderer.positionCount = N_TRAJECTORY_POINTS;
        lineRenderer.enabled = false;
        minimaplineRenderer.positionCount = N_TRAJECTORY_POINTS;
        minimaplineRenderer.enabled = false;
        audioSource = gameObject.GetComponent<AudioSource>();

    }

    void Update()
    {
        if (!manager.getIsPaused())
        {
            if (!AllowToShoot) return;

            if (currentBullet == null)
            {
                if (Input.GetMouseButtonDown(0))
                {

                    _pressingMouse = true;
                    lineRenderer.enabled = true;
                    minimaplineRenderer.enabled |= true;
                }

                if (Input.GetMouseButtonUp(0))
                {
                    _pressingMouse = false;
                    lineRenderer.enabled = false;
                    minimaplineRenderer.enabled = false;
                    _Fire();
                    manager.setIsShooting(true);
                }

                if (_pressingMouse)
                {
                    mousePos = Input.mousePosition;
                    mousePos.z = 10;
                    mousePos = _cam.ScreenToWorldPoint(mousePos);
                    // look at
                    RaycastHit hit;
                    ray = _cam.ScreenPointToRay(Input.mousePosition);
                    if (Physics.Raycast(ray, out hit, Mathf.Infinity))
                    {
                        if (hit.collider.CompareTag("Bloqueador"))
                        {
                            return;
                        }
                    }
                    transform.LookAt(mousePos);

                    _initialVelocity = mousePos - firePoint.position;
                    float additionalForce = 3.5f; // Ajusta este valor seg�n tus necesidades
                    force = _initialVelocity * additionalForce;
                    force.z = 0;

                    _UpdateLineRenderer();
                    _UpdateMinimapLineRenderer();
                }
            }
        }
        else
        {
            return;
        }
            
       
    }

    
        

    private void _Fire()
    {
        // instantiate a cannon ball
        GameObject cannonBall = Instantiate(cannonBallPrefab, firePoint.position, Quaternion.identity);
        currentBullet = cannonBall;
        // apply some force
        Rigidbody rb = cannonBall.GetComponent<Rigidbody>();
        rb.AddForce(force, ForceMode.Impulse);
        audioSource.Play();
    }
    private void _UpdateLineRenderer()
    {
        float g = Physics.gravity.magnitude;
        float velocity = force.magnitude;
        float angle = Mathf.Atan2(force.y, force.x);

        Vector3 start = firePoint.position;

        float timeStep = 0.1f;
        float fTime = 0f;
        for (int i = 0; i < N_TRAJECTORY_POINTS; i++)
        {
            float dx = velocity * fTime * Mathf.Cos(angle);
            float dy = velocity * fTime * Mathf.Sin(angle) - (g * fTime * fTime / 2f);
            Vector3 pos = new Vector3(start.x + dx, start.y + dy, 0);
            lineRenderer.SetPosition(i, pos);
            fTime += timeStep;
        }
    }
    private void _UpdateMinimapLineRenderer()
    {
        float g = Physics.gravity.magnitude;
        float velocity = force.magnitude;
        float angle = Mathf.Atan2(force.y, force.x);

        Vector3 start = firePoint.position;

        float timeStep = 0.1f;
        float fTime = 0f;
        for (int i = 0; i < N_TRAJECTORY_POINTS; i++)
        {
            float dx = velocity * fTime * Mathf.Cos(angle);
            float dy = velocity * fTime * Mathf.Sin(angle) - (g * fTime * fTime / 2f);
            Vector3 pos = new Vector3(start.x + dx, start.y + dy, 0);
            minimaplineRenderer.SetPosition(i, pos);
            fTime += timeStep;
        }
    }
}
