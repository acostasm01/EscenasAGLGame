using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BarcoProta : MonoBehaviour
{
    public GameObject barco;
    private static JuegoBarcoGameManager manager;
    // Start is called before the first frame update
    void Start()
    {
        manager = JuegoBarcoGameManager.instance;
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Proa"))
        {
            Destroy(barco);
            manager.Death();
        }
        else return;
    }
}