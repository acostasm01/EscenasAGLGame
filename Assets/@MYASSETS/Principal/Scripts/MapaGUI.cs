using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapaGUI : MonoBehaviour
{
    [Header("References")]
    public Transform SpawnCeldasMT;
    public Transform SpawnLinares;
    public Transform SpawnPabellon;
    public Transform SpawnN1;
    public GameObject Player;
    public GameObject Menu;
    private AGLGameManager gameManager;

    private void Start()
    {
        gameManager = AGLGameManager.instance;
    }
    public void Teleport(int index)
    {
        switch (index){
            case 0:
                Player.transform.position = SpawnCeldasMT.position;
                break;
            case 1:
                Player.transform.position = SpawnPabellon.position;
                break;
            case 2:
                Player.transform.position = SpawnLinares.position;
                break;
            case 3:
                Player.transform.position = SpawnN1.position;
                break;
        }
        ObjectsHandle();
    }
    public void ShowMap()
    {
        Menu.SetActive(false);
        gameObject.SetActive(true);
    }
    private void ObjectsHandle()
    {
        gameObject.SetActive(false);
        gameManager.isPaused = false;
    }
}
