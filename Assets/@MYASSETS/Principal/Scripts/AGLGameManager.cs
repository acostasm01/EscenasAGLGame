using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AGLGameManager : MonoBehaviour
{
    public GameObject Menu;
    public PlayerCam playerCam;
    public Transform Player;
    public Vector3 lastPosition { get; set; }

    public bool isPaused { get; set; } = false;
    #region Singleton

    //Use a property so other code can't assign instance.
    static AGLGameManager _instance;
    public static AGLGameManager instance
    {
        get
        {
            if (!_instance)
                _instance = new GameObject("AGLGameManager", typeof(AGLGameManager)).GetComponent<AGLGameManager>();
            return _instance;
        }
    }

    void EnsureSingleton()
    {
        if (!_instance)
            _instance = this;

        var gameManagerInstances = GameObject.FindObjectsOfType<AGLGameManager>();

        if (gameManagerInstances.Length != 1)
        {
            Debug.LogError("Only one instance of the GameManager manager should ever exist. Removing extraneous.");

            foreach (var otherInstance in gameManagerInstances)
            {
                if (otherInstance != instance)
                {

                    if (otherInstance.gameObject == instance.gameObject)
                        Destroy(otherInstance);
                    else
                        Destroy(otherInstance.gameObject);
                }
            }
        }

    }
    #endregion Singleton

    void Awake()
    {
        EnsureSingleton();
    }
    // Start is called before the first frame update
    void Start()
    {
        Time.timeScale = 1f;
        Physics.gravity = new Vector3(0, -60f, 0);
    }

    // Update is called once per frame
    void Update()
    {
        
        TogglePause();
        if (Input.GetKeyDown(KeyCode.Escape) && !isPaused)
        {
            Menu.SetActive(true);
            isPaused = true;
        }
    }

    public void TogglePause()
    {
        if (isPaused)
        {
            playerCam.enabled = false;
            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;
        }
        else
        {
            playerCam.enabled = true;
            Cursor.lockState = CursorLockMode.Locked;
            Cursor.visible = false;
        }
    }
    public void setPlayerPosition()
    {
        PlayerPrefs.SetFloat("PlayerX", Player.position.x);
        PlayerPrefs.SetFloat("PlayerY", Player.position.y);
        PlayerPrefs.SetFloat("PlayerZ", Player.position.z + 2);
        PlayerPrefs.Save();

    }
}
