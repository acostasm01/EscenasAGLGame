using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using UnityEditor;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MechaGameManager : MonoBehaviour
{
    [Header("References")]
    public MechaPlayerCam playerCam;
    public Grappling grapple;
    public GameObject menuPause;
    public GameObject player;
    public Transform lastCheckpoint;
    public FallingPlatform fp;
    public LastZone lz;
    public bool isInLastZone { set; get; } = false;

    [Header("Verifiers")]
    public bool hasDied = false;
    public bool isPaused { get; set; } = false;
    public int Health { get; set; } = 100;
    private int maxHealth = 100; 
    
    #region Singleton

    //Use a property so other code can't assign instance.
    static MechaGameManager _instance;
    public static MechaGameManager instance
    {
        get
        {
            if (!_instance)
                _instance = new GameObject("MechaGameManager", typeof(MechaGameManager)).GetComponent<MechaGameManager>();
            return _instance;
        }
    }

    void EnsureSingleton()
    {
        if (!_instance)
            _instance = this;

        var gameManagerInstances = GameObject.FindObjectsOfType<MechaGameManager>();

        if (gameManagerInstances.Length != 1)
        {
            Debug.LogError("Only one instance of the GameManager manager should ever exist. Removing extraneous.");

            foreach (var otherInstance in gameManagerInstances)
            {
                if (otherInstance != instance)
                {

                    if (otherInstance.gameObject == instance.gameObject)
                        Destroy(otherInstance);
                    else
                        Destroy(otherInstance.gameObject);
                }
            }
        }

    }
    #endregion Singleton
    private void Awake()
    {
        EnsureSingleton();
        Physics.gravity = new Vector3(0, -30f, 0);
    }
    void Update()
    {
        TogglePause();
        if (Input.GetKeyDown(KeyCode.Escape) && !isPaused)
        {
            menuPause.SetActive(true);
            isPaused = true;
        }
    }
    public void TogglePause()
    {
        if (isPaused)
        {
            Time.timeScale = 0f;
            playerCam.enabled = false;
            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;
        }
        else
        {
            Time.timeScale = 1f;
            playerCam.enabled = true;
            Cursor.lockState = CursorLockMode.Locked;
        }
    }
    public void ReceiveDamage(int Damage)
    {
        Health -= Damage;   
        if(Health < 0)
        {
            Die();
        }
    }
    public void Heal(int curacion)
    {
        Health += curacion;

        if (Health > maxHealth)
        {
            Health = maxHealth; 
        }
    }
    public void KillEnemy(GameObject enemy)
    {
        Destroy(enemy);
    }
    public void NewCheckpoint(Transform newCheckpoint)
    {
        lastCheckpoint = newCheckpoint;
    }
    public void Respawn()
    {
        fp.ResetPlatform();
        player.transform.position = lastCheckpoint.position;
        Invoke(nameof(ResetGrappleOnDeath), 0.5f);
        

    }
    public void Die()
    {
        hasDied = true;
        

        Health = 100;
        Respawn();
        if(isInLastZone)
        {
            lz.onPlayerDeath();
        }
        
    }
    public void ResetScene()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }
    private void ResetGrappleOnDeath()
    {

        hasDied = false;
    }
}
