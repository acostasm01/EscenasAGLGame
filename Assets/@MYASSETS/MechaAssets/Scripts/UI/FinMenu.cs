using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class FinMenu : MonoBehaviour
{
    private MechaGameManager manager;
    private GameStateManager gameManager;

    // Start is called before the first frame update
    void Start()
    {
        manager = MechaGameManager.instance;   
        gameManager = GameStateManager.instance;
    }
    public void GameComplete()
    {
        gameManager.MarcarEscenaComoCompletada(SceneManager.GetActiveScene().buildIndex);
    }
    public void PlayAgain()
    {
        manager.ResetScene();
    }
}
