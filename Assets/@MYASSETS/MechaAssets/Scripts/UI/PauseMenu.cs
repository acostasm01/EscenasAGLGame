using DG.Tweening.Core.Easing;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class PauseMenu : MonoBehaviour
{
    private MechaGameManager manager;
    private GameStateManager gameManager;
    public GameObject menuPause;
    public GameObject menuSettings;
    public Slider slider;
    public TMP_Text sensText;
    // Start is called before the first frame update
    void Start()
    {
        manager = MechaGameManager.instance;
        gameManager = GameStateManager.instance;
    }
    public void Resume()
    {
        menuPause.SetActive(false);
        manager.isPaused = false;
    }
    public void Exit()
    {
        gameManager.QuitToMainScene();
    }
    public void Settings()
    {
        menuPause.SetActive(false);
        menuSettings.SetActive(true);
    }
    public void ChangeText()
    {
        sensText.text = slider.value.ToString();
    }
    public void Accept()
    {
        menuSettings.SetActive(false);
        menuPause.SetActive(true);
    }
}
