using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GUIMecha : MonoBehaviour
{
    [Header("References")]
    public Slider slider;
    private MechaGameManager manager;


    // Start is called before the first frame update
    void Start()
    {
        manager = MechaGameManager.instance;
    }

    // Update is called once per frame
    void Update()
    {
        slider.value = manager.Health;
    }
}
