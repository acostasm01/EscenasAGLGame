using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActivateLastZone : MonoBehaviour
{
    [Header("References")]
    public GameObject puerta;
    public LastZone lastZone;
    private MechaGameManager manager;

    private void Start()
    {
        manager = MechaGameManager.instance;
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag.Equals("Player"))
        {
            manager.isInLastZone = true;
            lastZone.startZone();
            puerta.SetActive(true);
        }
    }
}
