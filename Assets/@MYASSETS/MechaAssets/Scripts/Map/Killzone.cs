using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Killzone : MonoBehaviour
{
    private MechaGameManager manager;

    void Start()
    {
        manager = MechaGameManager.instance;
    }
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag.Equals("Player"))
        {
            manager.Die();
        }
    }
}
