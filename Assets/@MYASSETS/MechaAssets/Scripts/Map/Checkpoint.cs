using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Checkpoint : MonoBehaviour
{
    [Header("References")]
    public Transform checkpoint;
    private MechaGameManager manager;
    public ActivarCheckPoint activarCheckpoint;

    // Start is called before the first frame update
    void Start()
    {
        manager = MechaGameManager.instance;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag.Equals("Player"))
        {
            manager.NewCheckpoint(checkpoint);
            activarCheckpoint.enabled = true;
        }
    }
}
