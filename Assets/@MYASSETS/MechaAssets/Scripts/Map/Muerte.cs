using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Muerte : MonoBehaviour
{
    private MechaGameManager manager;

    void Start()
    {
        manager = MechaGameManager.instance;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag.Equals("Player"))
        {
            manager.Respawn();
        }
    }
}
