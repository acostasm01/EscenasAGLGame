using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FallingPlatform : MonoBehaviour
{
    [Header("References")]
    Vector3 origin;
    [Header("Falling")]
    public bool isFalling = false;
    float downSpeed = 0;

    private void Start()
    {
        origin= this.transform.position;
    }
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag.Equals("Player"))
        {
            isFalling = true;

        }
    }

    // Update is called once per frame
    void Update()
    {
        if (isFalling)
        {
            downSpeed += Time.deltaTime;
            transform.position = new Vector3(transform.position.x, transform.position.y - downSpeed, transform.position.z);
        }
        else
        {
            downSpeed = 0;
        }
    }
    public void ResetPlatform()
    {
        isFalling= false;
        transform.position = origin;
    }
}
