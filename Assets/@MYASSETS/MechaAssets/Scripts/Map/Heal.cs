using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Heal : MonoBehaviour
{
    [Header("References")]
    private Transform player;
    [Header("Curacion")]
    public int curacion;

    private MechaGameManager manager;
    private void Start()
    {
        manager = MechaGameManager.instance;
        player = GameObject.Find("PlayerObj").transform;
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag.Equals("Player"))
        {
            manager.Heal(curacion);
            Destroy(this.gameObject);
        }
    }
    private void Update()
    {
        if(player != null)
        {
            transform.LookAt(player.position);
        }
    }
}
