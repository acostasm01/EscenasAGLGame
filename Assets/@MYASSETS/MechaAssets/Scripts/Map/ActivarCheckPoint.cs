using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActivarCheckPoint : MonoBehaviour
{
    [Header("References")]
    public GameObject checkpoint;

    public Material materialEncendido;

    private void Start()
    {
        CambiarMaterial();
    }
    public void CambiarMaterial()
    {
            Renderer renderer = checkpoint.GetComponent<Renderer>();
            Material[] materiales = renderer.materials;
            materiales[1] = materialEncendido;
            renderer.materials = materiales;
    }
}
