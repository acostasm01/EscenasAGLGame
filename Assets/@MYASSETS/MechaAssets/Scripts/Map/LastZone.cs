using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class LastZone : MonoBehaviour
{
    [Header("References")]
    public GameObject timer;
    public TMP_Text timerText;
    public GameObject enemyKills;
    public TMP_Text enemyKillsText;
    public GameObject enemy;
    public GameObject[] spawnLocations;
    public GameObject healthLocation;
    public GameObject portal;
    public GameObject healthPack;
    public GameObject puerta;
    [Header("SpawnRate")]
    public float spawnCd;
    public float healthSpawnCd;

    [Header("Variables")]
    public float countdownTime;
    public int enemyDeaths { set; get; }
    private IEnumerator coroutine;
    private bool isBattling = false;

    public void startZone()
    {
        timer.SetActive(true);
        enemyKills.SetActive(true);
        Invoke(nameof(Spawn), 2f);
        Invoke(nameof(healthSpawn), 5f);
        coroutine = SpawnAtIntervals(spawnCd);
        keepSpawning = true;
        StartCoroutine(coroutine);
        isBattling=true;

    }
    public void onPlayerDeath()
    {
        StopCoroutine(coroutine);
        coroutine = null;
        countdownTime = 120;
        enemyDeaths = 0;
        timer.SetActive(false);
        enemyKills.SetActive(false);
        puerta.SetActive(false);
        keepSpawning = false;
        isBattling = false;
        GameObject[] enemyArray = GameObject.FindGameObjectsWithTag("EnemyLastZone");

        // Itera sobre todos los objetos encontrados
        foreach (GameObject enemy in enemyArray)
        {
            Destroy(enemy);
        }
    }

    // Update is called once per frame
    void Update()
    {
        if(isBattling)
        {
            if (countdownTime > 1)
            {
                countdownTime -= Time.deltaTime;
                float min = Mathf.FloorToInt(countdownTime / 60);
                float sec = Mathf.FloorToInt(countdownTime % 60);
                timerText.text = string.Format("{0:00}:{1:00}", min, sec < 10 ? "0" + sec.ToString() : sec.ToString());
                enemyKillsText.text = enemyDeaths.ToString();
            }
            else
            {
                keepSpawning = false;
                portal.SetActive(true);

            }
        }
    }
    private void Spawn()
    {
        HashSet<int> indicesElegidos = new HashSet<int>();

        while (indicesElegidos.Count < 3)
        {
            int indice = Random.Range(0, spawnLocations.Length);
            indicesElegidos.Add(indice);
        }

        foreach (int indice in indicesElegidos)
        {
            Instantiate(enemy, spawnLocations[indice].transform.position, Quaternion.identity);
        }
        
    }
    private void healthSpawn()
    {
        HealLastZone heal = FindObjectOfType<HealLastZone>();
        if (heal != null) return;
        else
        {
            Instantiate(healthPack, healthLocation.transform.position, Quaternion.identity);
        }
    }

    bool keepSpawning;

    IEnumerator SpawnAtIntervals(float secondsBetweenSpawns)
    {
        while (keepSpawning)
        {
            yield return new WaitForSeconds(secondsBetweenSpawns);

            Spawn();
        }
    }
    public void HealUsed()
    {
        Invoke(nameof(healthSpawn), 10f);
    }

}
