using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fin : MonoBehaviour
{
    [Header("References")]
    public GameObject menuFin;
    public GameObject GUI;
    private MechaGameManager manager;

    private void Start()
    {
        manager = MechaGameManager.instance;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag.Equals("Player"))
        {
           manager.isPaused = true;
           menuFin.SetActive(true);
           GUI.SetActive(false);
        }
    }
}
