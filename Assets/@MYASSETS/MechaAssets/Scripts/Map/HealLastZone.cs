using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealLastZone : MonoBehaviour
{
    [Header("References")]
    private Transform player;
    public LastZone lastZone;
    [Header("Curacion")]
    public int curacion;

    private MechaGameManager manager;
    private void Start()
    {
        manager = MechaGameManager.instance;
        player = GameObject.Find("PlayerObj").transform;
        lastZone = GameObject.Find("LastZone").GetComponent<LastZone>();
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag.Equals("Player"))
        {
            manager.Heal(curacion);
            lastZone.HealUsed();
            Destroy(this.gameObject);
        }
    }
    private void Update()
    {
        if (player != null)
        {
            transform.LookAt(player.position);
        }
    }
}
