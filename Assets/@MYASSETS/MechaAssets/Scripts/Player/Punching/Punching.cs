using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Punching : MonoBehaviour
{
    [Header("References")]
    public Collider Punch;
    public Animator animator;

    [Header("Cooldown")]
    public float punchingCd;
    private float punchingCdTimer;

    private float punchInput;

    private bool punching =false;

    // Update is called once per frame
    void Update()
    {
        punchInput = Input.GetAxisRaw("Fire1");
        if (punchInput != 0 && !punching ) { Attack(); }

        if (punching)
        {
            if (punchingCdTimer > 0)
                punchingCdTimer -= Time.deltaTime;
            else { 
                punching = false;
                Punch.enabled = false;
            }
                
        }
    }
    private void Attack()
    {
        if (punchingCdTimer > 0) return;
        Punch.enabled = true;
        punching = true;
        animator.SetTrigger("Punch");
        punchingCdTimer = punchingCd;

    }
}
