using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PunchEffect : MonoBehaviour
{
    [Header("References")]
    private MechaGameManager manager;
    private void Start()
    {
        manager = MechaGameManager.instance;
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag.Equals("Enemy") || other.gameObject.tag.Equals("EnemyLastZone"))
        {
            manager.KillEnemy(other.gameObject);
        }
    }
}
