using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Grappling : MonoBehaviour
{
    [Header("References")]
    private PlayerMovementAdvanced pm;
    public Transform cam;
    public Transform gunTip;
    public LayerMask whatIsGrappleable;
    public LayerMask whatIsEnergyScreen;
    public LineRenderer lr;
    public GameObject HandGrappleHead;
    public GameObject GrappleHead;
    public Transform orientation;
    public Animator animator;
    private MechaGameManager manager;


    [Header("Grappling")]
    public float maxGrappleDistance;
    public float grappleDelayTime;
    public float overshootYAxis;

    private Vector3 grapplePoint;

    [Header("Cooldown")]
    public float grapplingCd;
    private float grapplingCdTimer;

    private float grappleInput;

    private bool grappling;



    private void Start()
    {
        manager = MechaGameManager.instance;
        pm = GetComponent<PlayerMovementAdvanced>();
        
    }

    private void Update()
    {
        grappleInput = Input.GetAxisRaw("Fire2");
        if (grappleInput != 0 && !grappling) { 
            StartGrapple(); }

        if (grapplingCdTimer > 0)
            grapplingCdTimer -= Time.deltaTime;
    }

    private void LateUpdate()
    {
         if (grappling)
            lr.SetPosition(0, gunTip.position);
    }

    private void StartGrapple()
    {
        if (grapplingCdTimer > 0) return;

        grappling = true;

        pm.freeze = true;
        
        RaycastHit hit;
        if (Physics.Raycast(cam.position, cam.forward, out hit, maxGrappleDistance, whatIsEnergyScreen))
        {
            grapplePoint = hit.point;

            ThrowGrapple();

            Invoke(nameof(StopGrapple), grappleDelayTime);
        }
        else if (Physics.Raycast(cam.position, cam.forward, out hit, maxGrappleDistance, whatIsGrappleable))
        {
            grapplePoint = hit.point;

            ThrowGrapple();
            
            Invoke(nameof(ExecuteGrapple), grappleDelayTime);
        }
        
        else
        {
            grapplePoint = cam.position + cam.forward * maxGrappleDistance;
            ThrowGrapple();
            Invoke(nameof(StopGrapple), grappleDelayTime);
        }

        lr.enabled = true;
        lr.SetPosition(1, grapplePoint);
    }

    private void ExecuteGrapple()
    {
        pm.freeze = false;
        pm.activeGrapple = true;
        Vector3 lowestPoint = new Vector3(transform.position.x, transform.position.y - 1f, transform.position.z);

        float grapplePointRelativeYPos = grapplePoint.y - lowestPoint.y;
        float highestPointOnArc = grapplePointRelativeYPos + overshootYAxis;

        if (grapplePointRelativeYPos < 0) highestPointOnArc = overshootYAxis;
        

        if(manager.hasDied == true) { 
            Invoke(nameof(StopGrapple), 0.2f);
            pm.ResetVelocityOnDeath();
            return;
        }
        pm.JumpToPosition(grapplePoint, highestPointOnArc); 
            
        
    }
    public void ThrowGrapple()
    {
        HandGrappleHead.SetActive(false);
        animator.SetTrigger("Grapple");
        GrappleHead.transform.position = grapplePoint;
        GrappleHead.transform.rotation = orientation.rotation;
        GrappleHead.SetActive(true);
    }

    public void StopGrapple()
    {
        pm.activeGrapple = false;

        pm.freeze = false;

        grappling = false;

        grapplingCdTimer = grapplingCd;

        lr.enabled = false;

        GrappleHead.SetActive(false);

        HandGrappleHead.SetActive(true);
    }

    public bool IsGrappling()
    {
        return grappling;
    }

    public Vector3 GetGrapplePoint()
    {
        return grapplePoint;
    }
}