using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shield : MonoBehaviour
{
    public Shielding scriptShield;
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag.Equals("EnemyProjectile"))
        {
            Destroy(other.gameObject);
            scriptShield.BreakShield();
        }
    }
}
