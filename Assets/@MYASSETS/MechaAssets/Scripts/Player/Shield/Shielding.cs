using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shielding : MonoBehaviour
{
    [Header("References")]
    public GameObject Shield;
    public Animator animator;

    [Header("Shield")]
    public float shieldCastTime;
    private float shieldCastTimeTimer;
    public int shieldCountdown;
    public int shieldCountdownTimer;

    [Header("Input")]
    public KeyCode shieldKey = KeyCode.Q;
    private float shieldInput;
    private bool hadAnimated = false;
    public bool isHolding;

    // Update is called once per frame
    void Update()
    {
        if (Input.GetButtonDown("Shield"))
        {
            isHolding = true;
        }
        if ((Input.GetKey(shieldKey) || Input.GetButton("Shield")) && isHolding)
        {
            shieldCastTimeTimer += Time.deltaTime;
            if (shieldCastTimeTimer >= shieldCastTime)
            {
                if(!hadAnimated)
                {
                    animator.SetTrigger("Shield");
                    hadAnimated = true;
                }
                CastShield();
            }
        }

        if (Input.GetKeyUp(shieldKey) || Input.GetButtonUp("Shield"))
        {
            isHolding = false;
            StopCast();
        }
    }

    private void CastShield()
    {
        Shield.SetActive(true);
        
        shieldCastTimeTimer = 0f;
    }
    private void StopCast()
    {
        Shield.SetActive(false);
        hadAnimated = false;
    }
    public void BreakShield()
    {
        isHolding = false;
        Shield.SetActive(false);
        shieldCastTimeTimer = 0f;
        hadAnimated = false;
    }
}
