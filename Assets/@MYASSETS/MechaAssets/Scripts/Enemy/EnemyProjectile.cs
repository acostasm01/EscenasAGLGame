using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyProjectile : MonoBehaviour
{
    private MechaGameManager manager;
    [Header("Proyectile")]
    public int damage;
    private float lifetime = 2f;
    // Start is called before the first frame update
    void Start()
    {
        manager = MechaGameManager.instance;
        Destroy(gameObject, lifetime);
    }
    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag.Equals("Player"))
        { 
            manager.ReceiveDamage(damage);
        }
    }
}
