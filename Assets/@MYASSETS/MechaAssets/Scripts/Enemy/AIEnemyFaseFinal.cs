using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class AIEnemyFaseFinal : MonoBehaviour
{
    [Header("References")]
    public NavMeshAgent agent;
    public Transform player;
    public LayerMask whatIsGround, whatisPlayer;
    public LastZone lz;
    [Header("Patrolling")]
    public Vector3 walkPoint;
    bool walkPointSet;
    public float walkPointRange;

    [Header("Attacking")]
    public float attackCd;
    bool alreadyAttacked;
    public GameObject projectile;
    public float attackSpeed;

    [Header("States")]
    public float sightRange, attackRange;
    public bool playerInSightRange, playerInAttackRange;
    public bool patroling;
    public bool chasing;
    public bool attacking;

    private void Awake()
    {
        player = GameObject.Find("PlayerObj").transform;
        lz = GameObject.Find("LastZone").GetComponent<LastZone>();
        agent = GetComponent<NavMeshAgent>();
    }

    // Update is called once per frame
    void Update()
    {
        playerInAttackRange = Physics.CheckSphere(transform.position, attackRange, whatisPlayer);

        if (!playerInAttackRange) ChasePlayer();
        if (playerInAttackRange) AttackPlayer();

    }
    private void ChasePlayer()
    {
        agent.SetDestination(player.position);
        chasing = true;
        patroling = false;
        attacking = false;
    }
    private void AttackPlayer()
    {
        attacking = true;
        patroling = false;
        chasing = false;
        agent.SetDestination(transform.position);

        transform.LookAt(new Vector3(player.position.x, transform.position.y, player.position.z));

        if (!alreadyAttacked)
        {
            Rigidbody rb = Instantiate(projectile, new Vector3(transform.position.x, transform.position.y+1f, transform.position.z), transform.rotation).GetComponent<Rigidbody>();
            rb.AddForce(transform.forward * attackSpeed, ForceMode.Impulse);
            alreadyAttacked = true;
            Invoke(nameof(ResetAttack), attackCd);
        }
    }
    private void ResetAttack()
    {
        alreadyAttacked = false;
    }
    private void OnDestroy()
    {
        lz.enemyDeaths++;
    }
}
