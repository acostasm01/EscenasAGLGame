using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class AIEnemy : MonoBehaviour
{
    [Header("References")]
    public NavMeshAgent agent;
    public Transform player;
    public LayerMask whatIsGround, whatisPlayer;

    [Header("Patrolling")]
    public Vector3 walkPoint;
    bool walkPointSet;
    public float walkPointRange;

    [Header("Attacking")]
    public float attackCd;
    bool alreadyAttacked;
    public GameObject projectile;
    public float attackSpeed;

    [Header("States")]
    public float sightRange, attackRange;
    public bool playerInSightRange, playerInAttackRange;
    public bool patroling;
    public bool chasing;
    public bool attacking;

    private void Awake()
    {
        player = GameObject.Find("PlayerObj").transform;
        agent = GetComponent<NavMeshAgent>();
    }

    // Update is called once per frame
    void Update()
    {
        playerInSightRange = Physics.CheckSphere(transform.position, sightRange, whatisPlayer);
        playerInAttackRange = Physics.CheckSphere(transform.position, attackRange, whatisPlayer);

        if (!playerInSightRange && !playerInAttackRange) Patroling();
        if (playerInSightRange && !playerInAttackRange) ChasePlayer();
        if (playerInAttackRange && playerInSightRange) AttackPlayer();

    }

    private void Patroling()
    {
        if (!walkPointSet) SearchWalkPoint();

        if (walkPointSet)
        {
            agent.SetDestination(walkPoint);
            
        }

        Vector3 distanceToWalkPoint = transform.position - walkPoint;

        //Walkpoint reached
        if (distanceToWalkPoint.magnitude < 1f)
        {
            walkPointSet = false;
            patroling = false;
        }
            
    }
    private void SearchWalkPoint()
    {
        chasing = false;
        attacking = false;
        patroling = true;
        //Calculate random point in range
        float randomZ = Random.Range(-walkPointRange, walkPointRange);
        float randomX = Random.Range(-walkPointRange, walkPointRange);

        walkPoint = new Vector3(transform.position.x + randomX, transform.position.y, transform.position.z + randomZ);

        walkPointSet = true;

        Invoke(nameof(ResetMovement), 3f);
    }
    private void ChasePlayer()
    {
        agent.SetDestination(player.position);
        chasing = true;
        patroling = false;
        attacking = false;
    }
    private void AttackPlayer()
    {
        attacking = true;
        patroling = false;
        chasing = false;
        agent.SetDestination(transform.position);

        transform.LookAt(new Vector3(player.position.x, transform.position.y, player.position.z));

        if (!alreadyAttacked)
        {
            Rigidbody rb = Instantiate(projectile, new Vector3(transform.position.x, transform.position.y+1f, transform.position.z), transform.rotation).GetComponent<Rigidbody>();
            rb.AddForce(transform.forward * attackSpeed, ForceMode.Impulse);
            alreadyAttacked = true;
            Invoke(nameof(ResetAttack), attackCd);
        }
    }
    private void ResetAttack()
    {
        alreadyAttacked = false;
    }
    private void ResetMovement()
    {
        walkPointSet = false;
    }
}
