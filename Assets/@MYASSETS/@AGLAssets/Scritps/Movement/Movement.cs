using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Movement : MonoBehaviour
{
    [Header("Movement")]
    private float moveSpeed = 7;

    public float groundDrag;

    [Header("Ground Check")]
    public Transform groundCheck;
    private RaycastHit groundedHit;
    public float groundDistance = 0.2f;
    public float playerHeight;
    public LayerMask whatIsGround;
    public bool grounded;
    public float airMultiplier;

    float horizontalInput;
    float verticalInput;

    Vector3 moveDirection;

    Rigidbody rb;

    [Header("References")]
    public Transform orientation;
    private void Start()
    {
        rb = GetComponent<Rigidbody>();
        rb.freezeRotation = true;
        rb.drag = groundDrag;
        if(SceneManager.GetActiveScene().buildIndex == 0)
        {
            transform.position = new Vector3(PlayerPrefs.GetFloat("PlayerX"), PlayerPrefs.GetFloat("PlayerY"), PlayerPrefs.GetFloat("PlayerZ"));

        }
    }

    // Update is called once per frame
    void Update()
    {
        grounded = Physics.Raycast(groundCheck.position, -orientation.up, out groundedHit, groundDistance, whatIsGround);
        MyInput();
    }
    private void FixedUpdate()
    {
        MovePlayer();
        
    }
    private void MyInput()
    {
        horizontalInput = Input.GetAxisRaw("Horizontal");
        verticalInput = Input.GetAxisRaw("Vertical");
    }
    private void MovePlayer()
    {
        moveDirection = orientation.forward * verticalInput + orientation.right * horizontalInput;
        if (grounded)
            rb.AddForce(moveDirection.normalized * moveSpeed * 10f, ForceMode.Force);
        else if (!grounded)
            rb.AddForce(moveDirection.normalized * moveSpeed * 10f * airMultiplier, ForceMode.Force);
    }
    
}
