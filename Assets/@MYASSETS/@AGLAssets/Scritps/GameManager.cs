using System.Collections;
using System.Collections.Generic;
using System.IO;
using TMPro;
using UnityEditor;
using UnityEngine;
using UnityEngine.SceneManagement;


public class GameStateManager : MonoBehaviour
{
    
    public bool isMainScene { get; set; } = true;

    public List<GameState> sceneComplete = new List<GameState>();
    [Header("UIGameStateList")]
    public GameObject textPrefab;
    public GameObject checkMarkPrefab;
    public GameObject crossMarkPrefab;
    public GameObject lista2D;
    public GameObject lista3D;
    public GameObject listaExperiencia;
    public GameObject listaSimuladores;

    private GameObject canvasUIGameState;
    #region Singleton

    //Use a property so other code can't assign instance.
    static GameStateManager _instance;
    public static GameStateManager instance
    {
        get
        {
            if (!_instance)
                _instance = new GameObject("GameManager", typeof(GameStateManager)).GetComponent<GameStateManager>();
            return _instance;
        }
    }

    void EnsureSingleton()
    {
        if (!_instance)
            _instance = this;

        var gameManagerInstances = GameObject.FindObjectsOfType<GameStateManager>();

        if (gameManagerInstances.Length != 1)
        {
            Debug.LogError("Only one instance of the GameManager manager should ever exist. Removing extraneous.");

            foreach (var otherInstance in gameManagerInstances)
            {
                if (otherInstance != instance)
                {

                    if (otherInstance.gameObject == instance.gameObject)
                        Destroy(otherInstance);
                    else
                        Destroy(otherInstance.gameObject);
                }
            }
        }

    }
    #endregion Singleton
    private void Awake()
    {
        EnsureSingleton();
    }
    private void Start()
    {
        if (SceneManager.GetActiveScene().buildIndex == 0)
        {
            isMainScene = true;
        }
        else
        {
            isMainScene = false;
        }
        uiGameStateList();
        if (isMainScene) {
            canvasUIGameState = GameObject.FindGameObjectWithTag("GameStateList");
            CreateUIElements(); 
        };

    }
    public void ResetGame()
    {
        PlayerPrefs.DeleteAll();
        ChangeMainScene();
    }
    public void QuitToMainScene()
    {
        if (!isMainScene) 
        { 
            ChangeMainScene();
            
        }
        else
        {
            Application.Quit();
        }
    }
    private void ChangeMainScene()
    {
        SceneManager.LoadScene(0, LoadSceneMode.Single);
        

    }

    
    #region GamesHandler


    public void MarcarEscenaComoCompletada(int sceneIndex)
    {
        sceneIndex-=1;
        if (sceneIndex >= 0 && sceneIndex < sceneComplete.Count)
        {
            
            GameState aux= sceneComplete[sceneIndex];

            Debug.Log(aux.sceneName);
            aux.state=true;
            //Guardo el estado en un playerprefs para no perder el estado si cambio de escena
            PlayerPrefs.SetInt("Scene" + ++sceneIndex, 1);
            PlayerPrefs.Save();
            ChangeMainScene();
        }
        else
        {
            Debug.LogError("��ndice de escena fuera de rango!");
        }
    }
    public bool ObtenerEstadoDeEscena(int sceneIndex)
    {
        sceneIndex -= 1;
        if (sceneIndex >= 0 && sceneIndex < sceneComplete.Count)
        {
            return sceneComplete[sceneIndex].state;
        }
        else
        {
            Debug.LogError("��ndice de escena fuera de rango!");
            return false;
        }
    }
    public void uiGameStateList()
    {
        ////Cuento numero escenas       
        int sceneCount = SceneManager.sceneCountInBuildSettings;
        ////Por cada escena
        for (int i = 1; i < sceneCount; i++)
        {
            string sceneName = Path.GetFileNameWithoutExtension(SceneUtility.GetScenePathByBuildIndex(i));

            //Compruebo el estado de la escena 1 completa(true) 0 no completa(false)
            bool state = PlayerPrefs.GetInt("Scene" + i, 0) == 1;
            string category = DetermineCategory(Path.GetFullPath(SceneUtility.GetScenePathByBuildIndex(i)));
            sceneComplete.Add(new GameState(sceneName, state,category));
            Debug.Log(sceneName+" "+ state+" "+category+"Scene"+i);
        }
    }
    public void CreateUIElements()
    {
        
        float yOffset = 500f; // Ajuste vertical para evitar superposiciones

        foreach (var gameState in sceneComplete)
        {
            
            // Crear el texto con el nombre de la escena
            GameObject textObject = Instantiate(textPrefab, canvasUIGameState.transform);
            TextMeshProUGUI textMesh = textObject.GetComponent<TextMeshProUGUI>();
            textMesh.text = gameState.sceneName;
            textObject.transform.localPosition = new Vector3(700f, yOffset, 0f);

            // Crear la marca de verificaci�n o cruz dependiendo del estado
            GameObject markPrefab = gameState.state ? checkMarkPrefab : crossMarkPrefab;
            GameObject markObject = Instantiate(markPrefab, canvasUIGameState.transform);
            markObject.transform.localPosition = new Vector3(920f, yOffset, 0f); // Ajustar posici�n
            yOffset -= 60f; // Ajustar la posici�n vertical para el pr�ximo elemento
        }
    }
    private string DetermineCategory(string scenePath)
    {
        string parentFolderName = new DirectoryInfo(Path.GetDirectoryName(scenePath)).Name;

        return parentFolderName; // Por ahora, simplemente devolvemos el nombre de la carpeta padre como categor�a
    }
    #endregion



}
public class GameState
{
    public string sceneName { set; get; }
    public bool state { set; get; }
    public string category { get; }
    public GameState(string sceneName, bool state, string category)
    {
        this.sceneName = sceneName;
        this.state = state;
        this.category = category;
    }
}

