using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Menu : MonoBehaviour
{
    [Header("References")]
    //public PlayerCam playerCam;
    public GameObject menu;
    GameStateManager manager;
    AGLGameManager gameManager;
    // Start is called before the first frame update
    void Start()
    {
        manager = GameStateManager.instance;
        gameManager = AGLGameManager.instance;
    }

    public void Resume()
    {
        menu.SetActive(false);
        gameManager.isPaused = false;
    }
    public void Quit()
    {
        manager.QuitToMainScene();
    }
    public void Reset()
    {
        manager.ResetGame();
    }

}
