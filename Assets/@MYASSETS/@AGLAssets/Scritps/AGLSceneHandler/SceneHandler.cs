using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneHandler : MonoBehaviour
{
    [Header("Scene")]
    private AGLGameManager manager;
    private GameStateManager gameManager;
    [Tooltip("Este valor se cambia dependiendo de el indice de la escena a cambiar")]
    public int SceneIndex;
    
    void Start()
    {
        manager = AGLGameManager.instance;
        gameManager = GameStateManager.instance;
    }

    private void OnTriggerEnter(Collider other)
    {
        ChangeScene();
    }

    private void ChangeScene()
    {
        if(gameManager.ObtenerEstadoDeEscena(SceneIndex))
        {
            enabled = false;
            Debug.Log("Desactivo el script para reducir elementos en la escena");
        }
        else
        {
            manager.setPlayerPosition();
            SceneManager.LoadScene(SceneIndex, LoadSceneMode.Single);
            
        }
    }
}
