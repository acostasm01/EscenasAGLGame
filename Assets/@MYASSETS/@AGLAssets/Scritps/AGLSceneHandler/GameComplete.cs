using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameComplete : MonoBehaviour
{
    private GameStateManager manager;


    // Start is called before the first frame update
    void Start()
    {
        manager = GameStateManager.instance;
    }

    private void OnTriggerEnter(Collider other)
    {
        manager.MarcarEscenaComoCompletada(SceneManager.GetActiveScene().buildIndex);
    }
}
