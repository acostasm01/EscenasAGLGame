using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.UI;

public class Enemigo : MonoBehaviour
{
    public float velocidadPatrulla = 2f;
    public Transform player;
    public GameObject moneda;
    private int Health = 5;
    public Text enemigoText;
    //private float LastShoot;

    public static int monedasDropeadas = 0;
    public static int monedasMaxDrop = 2;

    

    [SerializeField] private float vida;
    private Animator animator;

    private void Start()
    {
        animator = GetComponent<Animator>();
        monedasDropeadas = 0;
    }
    private void Update()
    {
        if(player == null)
        {
            return; 
        } 
        Vector3 direction = player.position - transform.position;
        if(direction.x > 0.0f)
        {
            transform.localScale = new Vector3(1, 1f, 1f);
        }
        if(direction.x < 0.0f)
        {
            transform.localScale = new Vector3(-1f, 1f, 1f);
        }


        
    }
    public void TomarDa�o(float da�o)
    {
        vida -= da�o;
        if( vida < 0)
        {
            DerrotarEnemigo();
        }
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("SwordP"))
        {
            DerrotarEnemigo();
        }
        if (collision.gameObject.CompareTag("Player"))
        {

            Debug.Log("Auch");
        }
    }
    private void DerrotarEnemigo()
    {

        if(monedasDropeadas < monedasMaxDrop)
        {
            monedasDropeadas++;

            GenerarMonedas();
            Debug.Log(monedasDropeadas);
            enemigoText.text = "0" + monedasDropeadas;
        }
        //logica del enemigo
        Destroy(gameObject);
    }


    

    public void Hit()
    {
        Health -= 1;
        if(Health == 0)
        {
            Destroy(gameObject);
        }
    }
    private void GenerarMonedas()
    {
        //instanciar moneda en la posicion del enemigo
        Instantiate(moneda, transform.position, Quaternion.identity);
    }


    

    //resto code enemigo
}

