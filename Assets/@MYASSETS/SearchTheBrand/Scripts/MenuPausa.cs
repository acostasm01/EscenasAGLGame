using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MenuPausa : MonoBehaviour
{
    [SerializeField] private GameObject btnPausa;
    [SerializeField] private GameObject menuPausa;
    private GameStateManager gameStateManager;

    private void Start()
    {
        gameStateManager = GameStateManager.instance;
    }
    public void Pausa()
    {
        Time.timeScale = 0f;
        btnPausa.SetActive(false);
        menuPausa.SetActive(true);
    }
    public void Reanudar()
    {
        Time.timeScale = 1f;
        btnPausa.SetActive(true);
        menuPausa.SetActive(false);

    }
    public void Reiniciar()
    {
        Time.timeScale = 1f;
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        SearchTheBrand_GameManager.Instance.ResetearMonedas();
    }
    public void Cerrar()
    {
        gameStateManager.QuitToMainScene();
    }
    public void Opciones()
    {
        Debug.Log("OPCIONES...");
    }
}
