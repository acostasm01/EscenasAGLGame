using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CombateJugador : MonoBehaviour
{
    [SerializeField] private float vida;
    [SerializeField] private float maximoVida;
    [SerializeField] private BarraDeVida barraDeVida;
    // Start is called before the first frame update
    void Start()
    {
        vida = maximoVida;
       // barraDeVida.InicializarBarraDeVida(vida);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void TomarDa�o(float da�oAtaque)
    {
        vida -= da�oAtaque;
        barraDeVida.CambiarVidaActual(vida);
        if(vida < 0)
        {
            Destroy(gameObject);
        }
    }
}
