using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine.Video;

public class CharacterMove : MonoBehaviour
{
    public GameObject BulletPrefab;
    public float Speed;
    public float JumpForce;
    public int Lifes;
    public int Jumps = 2;
    public GameObject jugador;
    public static float Coins;
    public Canvas hudCanvas;
    public Image[] VidasImagens;
    public Text monedasText; // Referencia al objeto de texto para mostrar el n�mero de monedas recolectadas

    private bool Grounded;
    private Rigidbody2D rb;
    private float Horizontal;
    private int jumpsRemaining;
    readonly float smoothTime = 1;
    private Vector3 escalaDerecha = new(2.5288f, 2.5288f, 4.5288f);
    private Vector3 escalaIzquierda = new(-2.5288f, 2.5288f, 4.5288f);
    [SerializeField] private Transform controladorGolpe;
    [SerializeField] private float radioGolpe;
    [SerializeField] private float da�oGolpe;
    private Animator animator;

    public Color colorAlRecibirGolpe = Color.red;
    public float duracionDestello = 0.1f;

    private Renderer rend;
    private Color colorOriginal;
    private Rebelde rebel; //Clase rebelde
    public float monedaDrop;//moneda dropeada eneemigo


    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        jumpsRemaining = Jumps;
        Lifes = 5;
        Coins = 0;
        monedaDrop = 0;//moneda Dropeada enemigo
        ActualizarImagenesVida();
        animator = GetComponent<Animator>();
        rend = GetComponent<Renderer>();
        colorOriginal = rend.material.color;


    }


    // Update is called once per frame
    void Update()
    {
        Horizontal = Input.GetAxisRaw("Horizontal");

        animator.SetBool("Running", Horizontal != 0.0f);


        if (Horizontal > 0.0f)
        {
            //transform.localScale = new Vector3(2.5288f, 2.5288f, 2.5288f);
            transform.localScale = Vector3.Lerp(transform.localScale, escalaDerecha, smoothTime);
        }
        else if (Horizontal < 0.0f)
        {
            //transform.localScale = new Vector3(-2.5288f, 2.5288f, 2.5288f);
            transform.localScale = Vector3.Lerp(transform.localScale, escalaIzquierda, smoothTime);
        }

        Debug.DrawRay(transform.position, Vector3.down * 0.2f, Color.red);
        Grounded = Physics2D.Raycast(transform.position, Vector3.down, 0.1f, LayerMask.GetMask("Ground"));
        if (Physics2D.Raycast(transform.position, Vector3.down, 0.2f))
        {
            Grounded = true;
            //Debug.Log("Grounded: " + Grounded);
        }

        if (Input.GetKeyDown(KeyCode.Space) && Grounded && jumpsRemaining > 0)
        {
            Jump();
        }
        if (Input.GetButtonDown("Fire1"))
        {
            Golpe();
        }
        if (Lifes == 0)
        {
            Morir();
        }


    }

    private void FixedUpdate()
    {
        rb.velocity = new Vector2(Horizontal * Speed, rb.velocity.y);
    }


    private void Jump()
    {
        rb.velocity = new Vector2(rb.velocity.x, 0);
        rb.AddForce(Vector2.up * JumpForce);
        jumpsRemaining--;
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Ground"))
        {
            jumpsRemaining = Jumps;
        }
        if (collision.gameObject.CompareTag("pinchos"))
        {
            Morir();
        }

    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Enemigo"))
        {
            Lifes -= 1;
            rb.AddForce(Vector2.up * JumpForce);
            Debug.Log("Lifes: " + Lifes);
            rend.material.color = colorAlRecibirGolpe;
            Invoke("RestaurarColorOriginal", duracionDestello);
            ActualizarImagenesVida();
            if (Lifes <= 0)
            {
                Morir();
            }
        }
        if (collision.gameObject.CompareTag("FinalBoss"))
        {
            Lifes -= 1;
            rb.AddForce(Vector2.up * JumpForce * 2);
            Debug.Log("Lifes: " + Lifes);
            rend.material.color = colorAlRecibirGolpe;
            Invoke("RestaurarColorOriginal", duracionDestello);
            ActualizarImagenesVida();
            if (Lifes <= 0)
            {
                Morir();
            }
        }
        if (collision.gameObject.CompareTag("Moneda"))
        {
            RecolectarMoneda(collision.gameObject);
        }
    }
    void RestaurarColorOriginal()
    {
        // Restaurar el color original del objeto
        rend.material.color = colorOriginal;
    }
    private void Golpe()
    {
        animator.SetTrigger("Golpe");
        Collider2D[] objetos = Physics2D.OverlapCircleAll(controladorGolpe.position, radioGolpe);

        foreach (Collider2D colisionador in objetos)
        {
            if (colisionador.CompareTag("Enemigo"))
            {
                colisionador.transform.GetComponent<Enemigo>().TomarDa�o(da�oGolpe);
            }
            if (colisionador.CompareTag("FinalBoss"))
            {
                colisionador.transform.GetComponent<Enemigo>().TomarDa�o(da�oGolpe);
            }
        }


    }
    /*private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(controladorGolpe.position, radioGolpe);
    }*/
    void Morir()
    {
        Debug.Log("Has muerto");
        SearchTheBrand_GameManager.Instance.ResetearMonedas();
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        
    }
    public void ResetearMonedas()
    {
        // Restablecer la cantidad de monedas recolectadas y las monedas dropeadas
        Coins = 0;
        Enemigo.monedasDropeadas = 0;
    }
    void RecolectarMoneda(GameObject moneda)
    {
        Destroy(moneda);
    }
    private void ActualizarImagenesVida()
    {
        for (int i = 0; i < VidasImagens.Length; i++)
        {
            if (i < Lifes)
            {
                // Si la vida actual es menor que las vidas restantes, muestra la imagen de vida
                VidasImagens[i].enabled = true;
            }
            else
            {
                // Si no, oculta la imagen de vida
                VidasImagens[i].enabled = false;
            }
        }
    }
}