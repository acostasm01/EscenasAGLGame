using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuControles : MonoBehaviour
{
    // Start is called before the first frame update
    public Canvas menuControlesCanvas;
    public Canvas menuInicial;
    public Canvas fin;
    private GameStateManager gameStateManager;
    private void Start()
    {
        menuControlesCanvas.enabled = false;
        gameStateManager = GameStateManager.instance;
    }
    public void MostrarControles()
    {
        menuControlesCanvas.enabled = true;
        menuInicial.enabled = false;
   
    }
    public void OcultarControles()
    {
        menuInicial.enabled = true;
        menuControlesCanvas.enabled = false;
        Debug.Log("Controles");
    }
    public void OcultarFin()
    {
        menuInicial.enabled = true;
        fin.enabled = false;
        Debug.Log("FIN");
        SearchTheBrand_GameManager.Instance.ResetearMonedas();
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }
    public void Salir() {
        gameStateManager.MarcarEscenaComoCompletada(SceneManager.GetActiveScene().buildIndex);
    }
}
