using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlataformaMove : MonoBehaviour
{
    public Transform superior;
    public Transform inferior;
    public float velociad = 2f;

    private bool subiendo = true;

    void Update()
    {
        if (subiendo)
        {
            MoverPlataforma(superior.position);
        }
        else
        {
            MoverPlataforma(inferior.position);
        }
    }

    void MoverPlataforma(Vector3 destino)
    {
        transform.position = Vector3.MoveTowards(transform.position, destino, velociad * Time.deltaTime);

        if(transform.position == superior.position )
        {
            subiendo = false;
        }
        else if( transform.position == inferior.position )
        {
            subiendo = true;
        }
    }
}
