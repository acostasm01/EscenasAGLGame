using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuInicial : MonoBehaviour
{
   public Canvas canvas;
    public GameObject menuControles;
    private GameStateManager gameStateManager;
    public static SearchTheBrand_GameManager Instance;

    private void Start()
    {
        gameStateManager = GameStateManager.instance;
    }
    public void Jugar()
    {
        
        if (canvas != null)
        {
            canvas.enabled = false;
            Time.timeScale = 1.0f;
        }
        else
        {
            Debug.LogError("No se encontro el componente Canvas en este objeto");
        }
    }

    public void Controles()
    {
        if(canvas != null)
        {
            menuControles.SetActive(true);
            if(canvas != null)
            {
                canvas.enabled = false;
            }
            else
            {
                Debug.Log("No se encontro el componente en este objeto");
            }
            Debug.Log("Controles");

        }
    }
    public void Salir()
    {
            gameStateManager.QuitToMainScene();
    }

    
}
