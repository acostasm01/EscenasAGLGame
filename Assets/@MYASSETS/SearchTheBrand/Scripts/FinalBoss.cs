using System;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.UI;

public class FinalBoss : MonoBehaviour
{
    
    public Rigidbody2D rb;
    public Transform jugador;
    [SerializeField] float vida;
    public Canvas fin;
    public GameObject personaje;
    public float velocidad;
    public Transform controladorSuelo;
    public float distancia;
    private bool moviendoDerecha;
    public float JumpForce;
    public int Lifes;
    public Color colorAlRecibirGolpe = Color.red;
    public float duracionDestello = 0.1f;
    private Renderer rend;
    private Color colorOriginal;
    private SpriteRenderer spriteRenderer;
    //private bool mirandoDerecha = true;
    //[Header("Vida")]
    //[SerializeField] private float vida;
    //[SerializeField] private BarraDeVida barraDeVida;
    //[SerializeField] float duracionCuentaAtras = 5f;
    [SerializeField] Text textoCuentaAtras;
    //private float tiempoRestante;
    //private bool cuentaAtrasTerminada = false;
    private void Start()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
        spriteRenderer.flipX = !spriteRenderer.flipX;
        //animator = GetComponent<Animator>();
        rb = GetComponent<Rigidbody2D>();
        //barraDeVida.InicializarBarraDeVida(vida);
        jugador = GameObject.FindGameObjectWithTag("Player").GetComponent<Transform>();
        //tiempoRestante = duracionCuentaAtras;
    }

    
    /*private void TerminarJuego()
    {
        
        Debug.Log("�Has derrotado al jefe final!");

        if (fin != null)
        {
            fin.enabled = true;
            Time.timeScale = 0.0f;
        }
        else
        {
            Debug.LogError("No se encontro el Canvas de fin en el objeto");
        }
    }*/

    private void Girar()
    {
        moviendoDerecha = !moviendoDerecha;
        transform.eulerAngles = new Vector3(0, transform.eulerAngles.y + 180, 0);
        velocidad *= -1;
    }

    private void FixedUpdate()
    {
        RaycastHit2D informacionSuelo = Physics2D.Raycast(controladorSuelo.position, Vector2.down, distancia);
        rb.velocity = new Vector2(velocidad, rb.velocity.y);
        if (informacionSuelo == false)
        {
            //Girar
            Girar();
        }
    }
    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawLine(controladorSuelo.transform.position, controladorSuelo.transform.position + Vector3.down * distancia);
    }
    //public void TomarDa�o(float da�o)
    //{
    //vida -= da�o;

    //}
    

}
