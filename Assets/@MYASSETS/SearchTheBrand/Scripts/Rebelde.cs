using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Rebelde : MonoBehaviour
{
    // Start is called before the first frame update
    public GameObject personaje;
    public float velocidad;
    public Transform controladorSuelo;
    public float distancia;
    private bool moviendoDerecha;
    private Rigidbody2D rb;
    public GameObject moneda;
    
    public static int monedasDropeadas = 0;
    public static int monedasMaxDrop = 2;
    [SerializeField] private float vida;
    public Text enemigoText;
    //private Animator anim;


    private void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        monedasDropeadas = 0;
        //anim = GetComponent<Animator>();
        
    }
    /*private void Update()
    {
        Vector3 direction = personaje.transform.position - transform.position; 
        if(direction.x >= 0)
        {
            transform.localScale = new Vector3(1.0f, 1.0f, 1.0f);
        }
        else
        {
            transform.localScale = new Vector3(-1.0f, 1.0f, 1.0f);
        }
    }*/

    private void FixedUpdate()
    {
        RaycastHit2D informacionSuelo = Physics2D.Raycast(controladorSuelo.position, Vector2.down, distancia);
        rb.velocity = new Vector2(velocidad, rb.velocity.y);
        if (informacionSuelo == false)
        {
            //Girar
            Girar();
        }
    }
    private void Girar()
    {
        moviendoDerecha = !moviendoDerecha;
        transform.eulerAngles = new Vector3(0, transform.eulerAngles.y + 180, 0);
        velocidad *= -1;
    }
    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawLine(controladorSuelo.transform.position, controladorSuelo.transform.position + Vector3.down * distancia);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("SwordP"))
        {
            DerrotarEnemigo();
        }
        if (collision.gameObject.CompareTag("Player"))
        {

            Debug.Log("Auch");
        }
    }

    private void DerrotarEnemigo()
    {

        if (monedasDropeadas == monedasMaxDrop)
        {
            monedasDropeadas = monedasDropeadas + 1;

            GenerarMonedas();
            Debug.Log(monedasDropeadas);
            enemigoText.text = "0" + monedasDropeadas;
        }
        //logica del enemigo
        Destroy(gameObject);
    }
    private void GenerarMonedas()
    {
        Debug.Log("Coin");
        //instanciar moneda en la posicion del enemigo
        Instantiate(moneda, transform.position, Quaternion.identity);
    }
    //--------------------------------------------------------------------
    
    


}
