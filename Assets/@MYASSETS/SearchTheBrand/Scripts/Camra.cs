using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Camra : MonoBehaviour
{
    public Transform jugador;
    public float suavizado = 0.1f;

    // Update is called once per frame
    void Update()
    {
        if (jugador != null)
        {
            // Calcula la posici�n objetivo de la c�mara
            Vector3 posicionObjetivo = new(jugador.position.x, jugador.position.y, transform.position.z);

            // Usa Lerp para suavizar el seguimiento
            transform.position = Vector3.Lerp(transform.position, posicionObjetivo, suavizado);
        }
    }
}
