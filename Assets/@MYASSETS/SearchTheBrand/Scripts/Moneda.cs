using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Moneda : MonoBehaviour
{
    public AudioClip sound;
    public int valorMoneda = 1;
    public GameObject jugador;

    private void Awake()
    {
        jugador = GameObject.FindGameObjectWithTag("Player");
    }
    void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            Debug.Log("Colision");
            RecolectarMoneda();
        }
    }

    private void RecolectarMoneda()
    {
        Camera.main.GetComponent<AudioSource>().PlayOneShot(sound);
        Destroy(gameObject);
        SearchTheBrand_GameManager.Instance.AgregarMonedas(valorMoneda);
    }
}