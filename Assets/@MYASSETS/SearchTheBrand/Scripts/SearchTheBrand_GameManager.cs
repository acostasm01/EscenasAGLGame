using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class SearchTheBrand_GameManager : MonoBehaviour
{
    public static SearchTheBrand_GameManager Instance;
    private GameStateManager stateManager;
    public int monedasRecogidas = 0;
    public int enemigosEliminados = 0;
    private int vidas = 5;
    private bool TodasLasmonedasRecogidas = false;
    public Transform tp;
    public Transform tpInicio;
    public string salaDelJugador = "salaJefe";
    public Canvas fin;
    public Canvas cuentaAtras;
    [SerializeField] float duracionCuentaAtras = 30f;
    private float tiempoRestante;
    private bool cuentaAtrasIniciada = false;
    public Text cuentaText;
    public Text sobreviveText;
    public Text monedasText;
    public Canvas canvas;
    [SerializeField] private GameObject btnPausa;
    [SerializeField] private GameObject menuPausa;
    public bool gameFinished;

    private void Start()
    {
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
        Time.timeScale = 0.0f;
        fin.enabled = false;
        cuentaAtras.enabled = false;
        stateManager = GameStateManager.instance;


    }
    public void Pausa()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Time.timeScale = 0f;
            btnPausa.SetActive(false);
            menuPausa.SetActive(true);
        }
    }
    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }
    
    public void AgregarMonedas(int cantidad)
    {
        
        monedasRecogidas += cantidad;
        
        Debug.Log("Monedas recogidas " + monedasRecogidas);

        monedasText.text ="0" +monedasRecogidas;
        if (monedasRecogidas >= 5 && !TodasLasmonedasRecogidas)
        {
            TodasLasMonedasRecogidas();
            Debug.Log("Tepeando");
        }
    }

    public void RestarVidas()
    {
        vidas--;
        Debug.Log("Vidas restantes: " + vidas);
        if (vidas <= 0)
        {
            //Logica para la derrota
            Debug.Log("El jugador ha sido derrotado");
        }
    }

    //PARA IMPLEMENTAR EL TP EN LA SALA DEL JEFE
    public void TodasLasMonedasRecogidas()
    {
        TodasLasmonedasRecogidas = true;
        GameObject[] jugadores = GameObject.FindGameObjectsWithTag("Player");
        foreach (GameObject jugador in jugadores)
        {
            jugador.transform.position = tp.position;
        }

        // Iniciar la cuenta regresiva despu�s de teletransportar al jugador
        IniciarCuentaAtras();
        
    }

    private void IniciarCuentaAtras()
    {
        // Inicializar el tiempo restante
        tiempoRestante = duracionCuentaAtras;
        cuentaAtrasIniciada = true;
        cuentaAtras.enabled = true;
        
        if(cuentaText != null)
        {
            cuentaText.text = "Tiempo: " + tiempoRestante.ToString("F0"); 
        }
        if(sobreviveText != null)
        {
            sobreviveText.text = "SOBREVIVE";
        }
    }

    private void Update()
    {

        if (cuentaAtrasIniciada)
        {
            // Actualizar el tiempo restante
            tiempoRestante -= Time.deltaTime;

            if (cuentaText != null)
            {
                cuentaText.text = "Tiempo: " + tiempoRestante.ToString("F0");
            }
            // Verificar si la cuenta regresiva ha terminado
            if (tiempoRestante <= 0)
            {
                cuentaAtrasIniciada = false;
                TerminarJuego();
            }
        }
    }

    private void TerminarJuego()
    {
        Debug.Log("�Has derrotado al jefe final!");

        if (fin != null)
        {
            fin.enabled = true;
            Time.timeScale = 0.0f;
            gameFinished = true;
            ResetearMonedas();
            GameObject[] jugadores = GameObject.FindGameObjectsWithTag("Player");
            foreach (GameObject jugador in jugadores)
            {
                jugador.transform.position = tpInicio.position;
            }
        }
        else
        {
            Debug.LogError("No se encontro el Canvas de fin en el objeto");
        }
    }
    public void ResetearMonedas()
    {
        // Restablecer la cantidad de monedas recolectadas y las monedas dropeadas
        CharacterMove.Coins = 0;
        Enemigo.monedasDropeadas = 0;
    }
    

}
