using UnityEngine;

public class Camera_Rotation : MonoBehaviour
{
    public float sensibilidad = 100;
    public Transform player;

    private Transform objetoMirar;
    private int block = 0;
    private float xRotation;

    public void setBlock(int block)
    {
        this.block = block;
    }

    public void setObjetoMirar(Transform objetoMirar)
    {
        this.objetoMirar = objetoMirar;
    }

    // Update is called once per frame
    void Update()
    {
        if (block == 0)
        {
            var mouse_x = Input.GetAxisRaw("Mouse X") * Time.deltaTime * sensibilidad;
            var mouse_y = Input.GetAxisRaw("Mouse Y") * Time.deltaTime * sensibilidad;
            xRotation -= mouse_y;
            xRotation = Mathf.Clamp(xRotation, -90, 90);
            transform.localRotation = Quaternion.Euler(xRotation, 0, 0);
            player.Rotate(Vector3.up * mouse_x);
        }
        else if (block == 1)
        {
            transform.LookAt(objetoMirar);
        }
        else if (block == 2)
        {
            xRotation = -12;
            block = 0;
        }
    }
}
