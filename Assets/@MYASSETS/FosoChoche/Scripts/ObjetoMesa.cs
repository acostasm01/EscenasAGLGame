using UnityEngine;

public class ObjetoMesa : MonoBehaviour
{
    public GameObject objetoMesa;
    private EleccionMesa eleccion;
    public Quaternion rotacion;
    // Start is called before the first frame update
    void Start()
    {
        eleccion = objetoMesa.GetComponent<EleccionMesa>();
    }
    private void Update()
    {
        
    }
    public void Interactuado(RaycastHit hit)
    {
        if(hit.collider)
        {
            eleccion.elegirOpcion(hit.transform.gameObject);
        }
    }
}
