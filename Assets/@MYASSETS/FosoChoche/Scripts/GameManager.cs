using UnityEngine;

public class FosoGameManager : MonoBehaviour
{

    [SerializeField]
    MissionController misionInstance;
    public GameObject FinMenu;

    #region Singleton

    //Use a property so other code can't assign instance.
    static FosoGameManager _instance;
    public static FosoGameManager instance
    {
        get
        {
            if (!_instance)
                _instance = new GameObject("GameManager", typeof(FosoGameManager)).GetComponent<FosoGameManager>();
            return _instance;
        }
    }

    void EnsureSingleton()
    {
        if (!_instance)
            _instance = this;

        var gameManagerInstances = GameObject.FindObjectsOfType<FosoGameManager>();

        if (gameManagerInstances.Length != 1)
        {
            Debug.LogError("Only one instance of the GameManager manager should ever exist. Removing extraneous.");

            foreach (var otherInstance in gameManagerInstances)
            {
                if (otherInstance != instance)
                {

                    if (otherInstance.gameObject == instance.gameObject)
                        Destroy(otherInstance);
                    else
                        Destroy(otherInstance.gameObject);
                }
            }
        }

    }
    #endregion Singleton

    private void Awake()
    {
        EnsureSingleton();
    }

    public void misionCompleta(string misionKey)
    {
        misionInstance.missionState[misionKey] = true;
        var number = int.Parse(misionKey.Split(' ')[1]);
        searchControllerStart(number);
    }

    void searchControllerStart(int controllerNumber)
    {
        switch (controllerNumber)
        {
            case 9:
                misionInstance.missionTexts[$"Mision 11"].SetActive(true);
                break;
        }
    }

    public bool searchState(string mision)
    {
        return misionInstance.missionState[mision];
    }

    public GameObject searchMision(string mision)
    {
        return misionInstance.missionTexts[mision];
    }
    public void Fin()
    {
        FinMenu.SetActive(true);
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
        Time.timeScale = 0;
    }
}
