using System.Collections;
using TMPro;
using UnityEngine;

public class CharacterInteract : MonoBehaviour
{
    [SerializeField]
    float raycastDistanceObjects = 3;

    [SerializeField]
    Transform posicionPlayerTapa,
        props;

    [SerializeField]
    LayerMask objectLayer,
        defaultLayer;

    [SerializeField]
    TMP_Text textAviso;

    [SerializeField]
    Animator coche;

    [SerializeField]
    GameObject luces,
        mancha_aceite,
        entrada_foso;

    [SerializeField]
    GameObject[] puertas;

    [SerializeField]
    Material manchaSepiolita;

    [SerializeField]
    EnterFoso enterFoso;

    GameObject lastSeen,
       grabedObject;

    string father_name;

    CharacterMovement characterMovement;

    bool avisoLuces = false;

    int ruedas = 0,
        mancha = 0;

    private void Awake()
    {
        characterMovement = transform.parent.GetComponent<CharacterMovement>();
    }

    void Update()
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;
        Debug.DrawRay(ray.origin, ray.direction * raycastDistanceObjects, Color.red);
        if (Physics.Raycast(ray, out hit, raycastDistanceObjects, objectLayer))
        {
            if (lastSeen != null && lastSeen.tag.Equals("Canvas") && !hit.collider.name.Equals(father_name)) { lastSeen.SetActive(false); lastSeen = null; }

            father_name = hit.collider.name;
            missionBlock(hit, raycastDistanceObjects);
        }

        else if (lastSeen != null && lastSeen.tag.Equals("Canvas")) { lastSeen.SetActive(false); lastSeen = null; }

        else if (grabedObject != null && Input.GetButtonDown("Interact")) degrab_object();
    }

    void missionBlock(RaycastHit hit, float distance)
    {
        bool active = false;
        switch (hit.transform.tag)
        {
            case "martillo":
                active = FosoGameManager.instance.searchState("Mision 1");
                break;
            case "carraca":
                active = true;
                break;
            case "casco":
                active = FosoGameManager.instance.searchState("Mision 2");
                break;
            case "tapones":
                active = FosoGameManager.instance.searchState("Mision 3");
                break;
            case "Tapa":
                active = FosoGameManager.instance.searchState("Mision 4");
                break;
            case "luces_foso":
                active = FosoGameManager.instance.searchState("Mision 5");
                break;
            case "puerta_garaje":
                active = FosoGameManager.instance.searchState("Mision 6");
                break;
            case "ruedas":
                active = FosoGameManager.instance.searchState("Mision 7");
                break;
            case "escape":
            case "dragger":
                active = FosoGameManager.instance.searchState("Mision 8");
                break;
            case "manchaAceite":
            case "sepiolita":
            case "escoba":
            case "recogedor":
            case "manchaSepiolita":
                active = FosoGameManager.instance.searchState("Mision 9");
                break;
            case "roto":
                active = FosoGameManager.instance.searchState("Mision 10");
                break;
        }

        if (active)
        {
            if (Input.GetButtonDown("Interact"))//Check if the player has pressed the Interaction button
                interaction(hit);

            if (hit.transform.childCount > 0 && !hit.transform.tag.Equals("Canvas"))
            {
                lastSeen = hit.transform.GetChild(0).gameObject;
                lastSeen.SetActive(true);
            }
        }
    }

    void interaction(RaycastHit hit)
    {

        switch (hit.collider.tag)
        {
            case "Tapa":
                interaccion_tapa(hit);
                break;

            case "martillo":
                grab_object(hit.transform.gameObject, 1);
                FosoGameManager.instance.misionCompleta("Mision 2");
                break;

            case "carraca":
                grab_object(hit.transform.gameObject, 2);
                FosoGameManager.instance.misionCompleta("Mision 1");
                break;

            case "casco":
                poner_ropa(hit.transform.gameObject);
                FosoGameManager.instance.misionCompleta("Mision 3");
                break;

            case "tapones":
                poner_ropa(hit.transform.gameObject);
                FosoGameManager.instance.misionCompleta("Mision 4");
                break;

            case "Boton":
                interaccion_boton(hit);
                break;

            case "puerta_garaje":
                hit.transform.Rotate(0, 0, 180);
                StartCoroutine(interaccion_interruptor_puertas());
                hit.transform.gameObject.layer = defaultLayer;
                break;

            case "luces_foso":
                hit.transform.GetChild(1).transform.Rotate(0, 0, 180);
                interaccion_interruptor_luces();
                break;

            case "ruedas":
                interaccion_ruedas(hit);
                break;

            case "escape":

                if (grabedObject != null)
                    if (grabedObject.tag.Equals("dragger"))
                    {
                        FosoGameManager.instance.misionCompleta("Mision 9");

                        foreach (var escape in GameObject.FindGameObjectsWithTag("escape"))
                            escape.SetActive(false);
                    }

                break;


            case "manchaAceite":

                if (grabedObject != null)
                    if (grabedObject.tag.Equals("sepiolita"))
                    {
                        hit.transform.GetComponent<Renderer>().material = manchaSepiolita;
                        hit.transform.tag = "manchaSepiolita";
                        enterFoso.beforeSepiolita = true;
                    }

                break;

            case "manchaSepiolita":
                if (grabedObject != null)
                {
                    var objeto = grabedObject.transform;
                    switch (grabedObject.tag)
                    {

                        case "escoba":
                            degrab_object();
                            Debug.Log(objeto.tag);
                            objeto.SetLocalPositionAndRotation(new Vector3(-42, -15, 15.32f), new Quaternion(0, 0, 0, 0));
                            mancha++;
                            break;
                        case "recogedor":
                            degrab_object();
                            var rotation = new Quaternion();
                            rotation.eulerAngles.Set(0, 50.94f, 0);
                            objeto.SetLocalPositionAndRotation(new Vector3(-39.86f, -14.91f, 13.48f), rotation);
                            mancha++;
                            break;
                    }
                    if (mancha == 2)
                    {
                        hit.collider.gameObject.SetActive(false);
                        enterFoso.sepiolita = true;
                    }
                }
                break;

            case "roto":
                FosoGameManager.instance.misionCompleta("Mision 11");
                break;

            default:
                grab_object(hit.transform.gameObject, 0);
                break;
        }
    }

    private void interaccion_ruedas(RaycastHit hit)
    {
        ruedas++;
        hit.transform.GetComponent<Collider>().enabled = false;

        if (ruedas == 4)
        {
            FosoGameManager.instance.misionCompleta("Mision 8");
        }
    }

    private void interaccion_interruptor_luces()
    {
        FosoGameManager.instance.misionCompleta("Mision 6");
        bool active = true;

        if (avisoLuces)
        {
            StartCoroutine(avisos("Es obligatorio trabajar en el foso con las luces encendidas"));
            avisoLuces = false;
            active = false;
        }
        else
            avisoLuces = true;

        for (int i = 0; i < luces.transform.childCount; i++)
            luces.transform.GetChild(i).gameObject.SetActive(active);
    }



    private IEnumerator interaccion_interruptor_puertas()
    {
        FosoGameManager.instance.misionCompleta("Mision 7");

        foreach (var p in puertas)
            p.GetComponent<Animator>().enabled = true;

        yield return new WaitForSecondsRealtime(0.6f);

        coche.enabled = true;
        mancha_aceite.SetActive(true);
        entrada_foso.SetActive(true);
    }

    private void poner_ropa(GameObject hit) { Destroy(hit.gameObject); }

    private void grab_object(GameObject objeto, int child)
    {
        objeto.GetComponent<Rigidbody>().isKinematic = true;
        switch (child)
        {
            case 1:
                objeto.transform.SetParent(transform.parent.GetChild(0));
                break;
            case 2:
                objeto.transform.SetParent(transform.parent.GetChild(1));
                break;
            default:
                degrab_object();
                objeto.transform.SetParent(transform.GetChild(0));
                grabedObject = objeto;
                break;
        }
        PosicionObjeto(objeto);
    }

    private void PosicionObjeto(GameObject objeto)
    {
        Vector3 pos = Vector3.zero;
        Quaternion rotacion = Quaternion.identity;
        switch (objeto.name)
        {
            case "martillo":
                rotacion = Quaternion.Euler(-90, -28, 0);
                pos = new Vector3(0, -0.305f, 0);
                break;
            case "Llave Carraca":
                rotacion = Quaternion.Euler(-180, 0, 0);
                break;
            case "Escoba":
                pos = new Vector3(0, -1.866f, 0.057f);
                break;
            case "Recogedor":
                pos = new Vector3(0, -1.8f, 0.52f);
                break;
        }
        objeto.transform.SetLocalPositionAndRotation(pos, rotacion);
    }



    private void degrab_object()
    {
        if (grabedObject == null) return;

        grabedObject.GetComponent<Rigidbody>().isKinematic = false;
        grabedObject.transform.SetParent(props);
        grabedObject = null;
    }



    private void interaccion_tapa(RaycastHit hit)
    {
        Destroy(hit.transform.GetChild(0).gameObject);
        StartCoroutine(characterMovement.positionMove(posicionPlayerTapa.position.x, posicionPlayerTapa.position.z, hit.transform));
        characterMovement.setBlock(false);
        hit.transform.GetComponent<TapaMovement>().enabled = true;
    }



    private void interaccion_boton(RaycastHit hit)
    {
        //hit.transform.GetComponent<Mission3Controller>().Mision3Complete();
    }

    IEnumerator avisos(string aviso)
    {
        textAviso.text = aviso;
        yield return new WaitForSecondsRealtime(2);
        textAviso.text = " ";
    }

}
