using System.Collections;
using UnityEngine;

public class CharacterMovement : MonoBehaviour
{
    public float speed,
        gravity;
    private bool block = true;

    public void setBlock(bool _block) { block = _block; }

    CharacterController characterController;
    void Start()
    {
        characterController = GetComponent<CharacterController>();
    }

    void Update()
    {
        var vertical = Input.GetAxisRaw("Vertical");
        var Horizontal = Input.GetAxisRaw("Horizontal");
        if (block)
            simple_gravity_modified((transform.right * Horizontal + transform.forward * vertical) * speed * Time.deltaTime);
        else if (vertical >= 0)
            simple_gravity_modified((Camera.main.transform.forward * vertical) * speed * Time.deltaTime);
    }

    private void simple_gravity_modified(Vector3 speed)
    {
        if (characterController.isGrounded)
            characterController.Move(speed);
        else
        {
            var move_gravity = speed + (new Vector3(0, -gravity, 0) * Time.deltaTime);
            characterController.Move(move_gravity);
        }
    }

    public IEnumerator positionMove(float x, float z, Transform position)
    {
        while (!(transform.position.x == x && transform.position.z == z))
        {
            transform.position = Vector3.MoveTowards(transform.position, new Vector3(x, transform.position.y, z), 0.1f);
            Vector3 targetPostition = new Vector3(position.position.x, transform.position.y, position.position.z);
            this.transform.LookAt(targetPostition);
            yield return null;
        }
        Camera.main.GetComponent<Camera_Rotation>().setBlock(1);
        Camera.main.GetComponent<Camera_Rotation>().setObjetoMirar(GameObject.FindGameObjectWithTag("Tapa").transform);
        TapaMovement.end = true;
    }
}
