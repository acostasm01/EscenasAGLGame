using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class OpcionesUI : MonoBehaviour
{
    bool active=false;
    public GameObject panel;
    private GameStateManager manager;
    private void Start()
    {
        manager = GameStateManager.instance;
    }
    // Update is called once per frame
    void Update()
    {
        if (Input.GetButtonDown("Cancel"))
        {
            if(active) reanudar();
            else
            {
                panel.SetActive(true);
                Time.timeScale = 0;
                Cursor.lockState = CursorLockMode.None;
                Cursor.visible = true;
                active = true;
            }
        }
    }

    public void reanudar()
    {
        panel.SetActive(false);
        Time.timeScale = 1;
        Cursor.lockState = CursorLockMode.Locked;
        active = false;
    }

    public void salir()
    {
        manager.QuitToMainScene();
    }
    public void PlayAgain()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }
    public void fin()
    {
        manager.MarcarEscenaComoCompletada(SceneManager.GetActiveScene().buildIndex);

    }
}
