using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartUI : MonoBehaviour
{
    private GameStateManager manager;
    // Start is called before the first frame update
    void Start()
    {
        manager = GameStateManager.instance;
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
        Time.timeScale = 0;
    }


    public void inicio()
    {
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
        Time.timeScale = 1;
        this.gameObject.SetActive(false);
    }

    public void salir()
    {
        manager.QuitToMainScene();
    }
}
