using UnityEngine;

public class Tope : MonoBehaviour
{
    public GameObject Character;
    private void OnTriggerEnter(Collider other)
    {
        TapaMovement.end = false;
        other.GetComponentInParent<TapaMovement>().enabled = false;
        other.GetComponentInParent<Rigidbody>().velocity = Vector3.zero;
        Character.GetComponent<CharacterMovement>().setBlock(true);
        Camera.main.GetComponent<Camera_Rotation>().setBlock(2);
        FosoGameManager.instance.misionCompleta("Mision 5");
    }
}
