using UnityEngine;
[RequireComponent(typeof(Rigidbody))]
public class TapaMovement : MonoBehaviour
{
    public float speed = 15f;
    Rigidbody rb;
    static public bool end = false;

    private void Awake()
    {
        if (GetComponent<TapaMovement>().isActiveAndEnabled)
            GetComponent<TapaMovement>().enabled = false;
    }

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        var vertical = Input.GetAxisRaw("Vertical");

        if (vertical >= 0 && end)
            rb.velocity = (-transform.forward * vertical) * speed;
    }
}
