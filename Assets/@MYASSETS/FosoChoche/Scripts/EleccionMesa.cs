using UnityEngine;

public class EleccionMesa : MonoBehaviour
{
    private bool llave;
    private bool martillo;
    private bool objetosCorrectosElegidos = false;
    private FosoGameManager manager;
    // Start is called before the first frame update
    void Start()
    {
        manager = FosoGameManager.instance;
    }
    void Update()
    {
        if (objetosCorrectosElegidos)
        {
            //manager.Mision1Completa();
            objetosCorrectosElegidos = false;
        }
    }
    public void elegirOpcion(GameObject go)
    {
        if (go.name.Equals("Llave Carraca"))
        {
            llave = true;
        }
        else if (go.name.Equals("martillo"))
        {
            martillo = true;
        }
        else
        {
            go.gameObject.SetActive(false);
            //Hacer saber al jugador que no es ese objeto
        }
        VerificarObjetosCorrectos();
    }

    private void VerificarObjetosCorrectos()
    {
        if (llave && martillo)
        {
            objetosCorrectosElegidos = true;
        }
    }
}
