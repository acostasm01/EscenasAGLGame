using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using TMPro;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.UI;

public class MissionController : MonoBehaviour
{
    [SerializeField]
    List<GameObject> textoMisiones;

    public Dictionary<string, GameObject> missionTexts = new Dictionary<string, GameObject>();
    public Dictionary<string, bool> missionState = new Dictionary<string, bool>();

    [SerializeField]
    Sprite cross, check;

    public Image Img_mision1;
    public Image Img_mision2;
    public Image Img_mision3;

    public TMP_Text mision1Text;
    public TMP_Text mision2Text;
    public TMP_Text mision3Text;

    private FosoGameManager manager;

    private void Start()
    {
        manager = FosoGameManager.instance;
        int indice = 1;
        foreach (var text in textoMisiones)
        {
            missionTexts.Add($"Mision {indice}", text);
            missionState.Add($"Mision {indice}", false);
            indice++;
        }

    }
    void Update()
    {
        string lastKey = "";
        foreach (var text in missionState)
        {
            if (!text.Value)
            {
                break;
                
            }
            lastKey = text.Key;
        }

        comprobarMisiones(lastKey);
    }

    private void comprobarMisiones(string key)
    {
        //Debug.Log(key);
        if (key.Equals("Mision 11"))
        {
            manager.Fin();
            return;
        }
        var exist_value = missionTexts.TryGetValue(key, out var text);
        if (exist_value && text.activeSelf)
            StartCoroutine(changeText(text));
        
    }

    IEnumerator changeText(GameObject text)
    {
        text.transform.GetChild(0).GetComponent<Image>().sprite = check;
        yield return new WaitForSecondsRealtime(1);
        text.SetActive(false);
    }
}
