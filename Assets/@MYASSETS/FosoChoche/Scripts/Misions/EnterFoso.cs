using UnityEngine;

public class EnterFoso : MonoBehaviour
{
    public bool sepiolita = false,
        beforeSepiolita = false;

    bool player = true,
        escoba = true,
        recogedor = true,
        sepiolitra = true;

    int salida = 0;
    private void OnTriggerEnter(Collider other)
    {
        if (player)
            if (other.gameObject.tag.Equals("Player"))
            {
                FosoGameManager.instance.searchMision("Mision 10").SetActive(true);
                player = false;
            }
    }

    private void OnTriggerExit(Collider other)
    {
        if (sepiolita)
            if (other.transform.tag.Equals("escoba") && escoba)
            {
                salida++;
                escoba = false;
            }
            else if (other.transform.tag.Equals("recogedor") && recogedor)
            {
                salida++;
                recogedor = false;
            }
        if (beforeSepiolita)
            if (other.transform.tag.Equals("sepiolita") && sepiolitra)
            {
                salida++;
                sepiolitra = false;
            }

        if (salida == 3)
        {
            FosoGameManager.instance.misionCompleta("Mision 10");
            sepiolita = false;
            beforeSepiolita = false;

        }
    }
}
