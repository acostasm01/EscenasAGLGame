using UnityEngine;
using TMPro;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class RunnerGameManager : MonoBehaviour
{
    //Inicializar las variables necesarias
    public static RunnerGameManager Instance { get; private set; }
    public float initialGameSpeed = 5f;
    public float gameSpeedIncrease = 0f;
    public float gameSpeed{ get; private set; }

    public TextMeshProUGUI gameOverText;
    public TextMeshProUGUI winText;
    public TextMeshProUGUI scoreText;
    public TextMeshProUGUI label1;
    public TextMeshProUGUI label2;
    public TextMeshProUGUI pause;
    public Button quit;
    public Button quitFin;
    public Image pestanaControl;
    public Image final;
    public Button retryButton;
    public Button playButton;
    public Button controllersButton;
    public AudioClip lose;
    public AudioClip win;


    private Player player;
    private Spawner spawner;
    private bool boolControler;

    private CapsuleCollider2D capsuleCollider2D;
    public float Score => score;

    private Vector3 tamañoOriginal;
    private bool juegoPausado = false;
    private bool juegoEmpezado = false;

    public float score;
    private GameStateManager manager;
    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            DestroyImmediate(gameObject);
        }
    }

    private void OnDestroy()
    {
        if (Instance == this)
        {
            Instance = null;
        }
    }
    //Cuando comienza el juego se desactivan todos los componentes y se inicializan los objetos
    private void Start()
    {
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
        player = FindObjectOfType<Player>();
        spawner = FindObjectOfType<Spawner>();
        capsuleCollider2D = player.GetComponent<CapsuleCollider2D>();
        score = 500f;
        tamañoOriginal = capsuleCollider2D.size;
        pestanaControl.gameObject.SetActive(false);
        label1.gameObject.SetActive(false);
        label2.gameObject.SetActive(false);
        player.gameObject.SetActive(false);
        scoreText.gameObject.SetActive(false);
        winText.gameObject.SetActive(false);
        gameOverText.gameObject.SetActive(false);
        retryButton.gameObject.SetActive(false);
        spawner.gameObject.SetActive(false);
        pause.gameObject.SetActive(false);
        boolControler = true;
        manager = GameStateManager.instance;
    }
    //Cuando comienza una nueva partida se define el objetivo del juego, se destruyen todos los objetos que pueda haber en pantalla y se inicializa la parte del canvas que haga falta
    public void NewGame()
    {
        Obstacle[] obstacles = FindObjectsOfType<Obstacle>();

        foreach (var obstacle in obstacles) {
            Destroy(obstacle.gameObject);
        }

        Camera.main.GetComponent<AudioSource>().Play();

        gameSpeed = initialGameSpeed;
        gameSpeedIncrease = 0.1f;
        score = 500f;
        enabled = true;
        juegoEmpezado = true;
        
        scoreText.gameObject.SetActive(true);
        playButton.gameObject.SetActive(false);
        controllersButton.gameObject.SetActive(false);
        player.gameObject.SetActive(true);
        label1.gameObject.SetActive(true);
        label2.gameObject.SetActive(true);
        spawner.gameObject.SetActive(true);
        winText.gameObject.SetActive(false);
        gameOverText.gameObject.SetActive(false);
        retryButton.gameObject.SetActive(false);
        pause.gameObject.SetActive(false);
        quit.gameObject.SetActive(false);
        quitFin.gameObject.SetActive(false);
        capsuleCollider2D.size = tamañoOriginal;

        // UpdateHiscore();
    }
    //Cuando se pierde se para el juego y se activa el texto de game over
    public void GameOver()
    {
        gameSpeed = 0f;
        enabled = false;

        Camera.main.GetComponent<AudioSource>().Stop();
        Camera.main.GetComponent<AudioSource>().PlayOneShot(lose);

        player.gameObject.SetActive(false);
        spawner.gameObject.SetActive(false);
        gameOverText.gameObject.SetActive(true);
        retryButton.gameObject.SetActive(true);

        // UpdateHiscore();
    }
    //Cuando se gana aparece el texto de win y el juego se para
    public void Win()
    {
        gameSpeed = 0f;
        enabled = false;

        Camera.main.GetComponent<AudioSource>().Stop();
        Camera.main.GetComponent<AudioSource>().PlayOneShot(win);

        player.gameObject.SetActive(false);
        spawner.gameObject.SetActive(false);
        winText.gameObject.SetActive(true);
        retryButton.gameObject.SetActive(true);
        quitFin.gameObject.SetActive(true);
    }
    public void QuitFin()
    {
        manager.MarcarEscenaComoCompletada(SceneManager.GetActiveScene().buildIndex);
    }
    //public void QuitFin()
    //{
    //    manager.MarcarEscenaComoCompletada(SceneManager.GetActiveScene().name);
    //}
    public void Quit()
    {
        manager.QuitToMainScene();
    }
    //Con esta funcion aparece y desaparece la informacion de los controles cada vez que se pulse el boton
    public void Controllers()
    {
        boolControler = !boolControler;

        playButton.gameObject.SetActive(boolControler);
        pestanaControl.gameObject.SetActive(!boolControler);
    }

    //Aumento de velocidad con el paso del tiempo, agregar la condicion del win y la funcion para pausar el juego
    private void Update()
    {
        gameSpeed += gameSpeedIncrease * Time.deltaTime;
        score -= gameSpeed * Time.deltaTime;
        scoreText.text = Mathf.FloorToInt(score).ToString();
        if(scoreText.text == "0"){
            Win();
        }
        if(juegoEmpezado == true){
            if (Input.GetKeyDown(KeyCode.Escape))
            {

                juegoPausado = !juegoPausado;

                if (juegoPausado)
                {
                    PausarJuego();
                }
                else
                {
                    ReanudarJuego();
                }
            }
            void PausarJuego()
            {
                Time.timeScale = 0f;
                pause.gameObject.SetActive(true);
                quit.gameObject.SetActive(true);
            }

            void ReanudarJuego()
            {
                Time.timeScale = 1f;
                pause.gameObject.SetActive(false);
                quit.gameObject.SetActive(false);
            }
        }
        
    }
    
    
    // private void UpdateHiscore(){
    //     float hiscore = PlayerPrefs.GetFloat("hiscore", 0);
    //     if(score > hiscore){
    //         hiscore = score;
    //         PlayerPrefs.SetFloat("hiscore", hiscore);
    //     }

    //     hiscoreText.text = Mathf.FloorToInt(hiscore).ToString("D5");
    // }
}
