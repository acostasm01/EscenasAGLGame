
using UnityEngine;

public class Obstacle : MonoBehaviour
{
    private float leftEdge;

    private void Start(){
        leftEdge = Camera.main.ScreenToWorldPoint(Vector3.zero).x - 2f;
    }
    private void Update(){
        transform.position += Vector3.left * RunnerGameManager.Instance.gameSpeed * Time.deltaTime * 1.1f;

        if(transform.position.x < leftEdge){
            Destroy(gameObject);
        }
    }
}
