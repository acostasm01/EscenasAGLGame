using UnityEngine;

public class Spawner : MonoBehaviour
{
    //Inicializo las variables que me van a hacer falta
    [System.Serializable]
    public struct SpawnableObject
    {
        public GameObject prefab;
        [Range(0f, 1f)]
        public float spawnChance;
    }
    public GameObject final;
    public SpawnableObject[] objects;

    private float minSpawnRate = 1f;
    private float maxSpawnRate = 2f;
    private float Score;
    private Spawner spawner;
    //Inicializo el componente
    private void Start(){
        spawner = FindObjectOfType<Spawner>();
    }
    //Inicializo el componente, le añado una velocidad inicial y hago que cuando queden 30 metros haga una funcion
    private void Update()
    {
        Score = FindObjectOfType<RunnerGameManager>().score;
        float speed = 14 / RunnerGameManager.Instance.gameSpeed / transform.localScale.x;
        maxSpawnRate = speed;
        if(Score < 30){
            SpawnFinal();
        }
    }

    private void OnEnable()
    {
        Invoke(nameof(Spawn), Random.Range(minSpawnRate, maxSpawnRate));
    }

    private void OnDisable()
    {
        CancelInvoke();
    }
    //Funcion para que los objetos vayan spawneando
    private void Spawn()
    {
        float spawnChance = Random.value;

        foreach (var obj in objects)
        {
            if (spawnChance < obj.spawnChance)
            {
                GameObject obstacle = Instantiate(obj.prefab);
                obstacle.transform.position += transform.position;
                break;
            }

            spawnChance -= obj.spawnChance;
        }

        Invoke(nameof(Spawn), Random.Range(minSpawnRate, maxSpawnRate));
    }
    //Funcion para que spawnee la bandera final
    private void SpawnFinal(){
        GameObject obstacle = Instantiate(final);
        spawner.gameObject.SetActive(false);
        obstacle.transform.position += transform.position;
    }
}