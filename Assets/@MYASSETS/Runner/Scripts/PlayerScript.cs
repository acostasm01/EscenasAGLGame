using UnityEngine;

public class Player : MonoBehaviour
{
    public AudioClip jump;
    public float JumpForce;

    private Rigidbody2D Rigidbody2D;
    private bool Grounded;
    private Animator Animator;
    private CapsuleCollider2D capsuleCollider2D;
    private Vector3 tamañoOriginal;
    void Start()
    {
        Rigidbody2D = GetComponent<Rigidbody2D>();
        Animator = GetComponent<Animator>();

        capsuleCollider2D = GetComponent<CapsuleCollider2D>();
        
        if (capsuleCollider2D == null)
        {
            enabled = false;
        }
        // Almacena el tamaño original del BoxCollider
        tamañoOriginal = capsuleCollider2D.size;
    }

    void Update()
    {

        //Aumentar la velocidad de la animación de correr

        float speed = RunnerGameManager.Instance.gameSpeed / transform.localScale.x;
        Animator.SetFloat("RunSpeed", speed/7);

        //Detectar si el personaje esta en el suelo o en el aire para poder saltar de nuevo

        if (Physics2D.Raycast(transform.position, Vector3.down, 0.8f))
        {
            Grounded = true;
        }
        else Grounded = false;

        //Activar la animacion de salto y descativarla cuando saltas

        if(Grounded == false){
            Animator.SetBool("Jump", true);
        } else if (Grounded == true){
            Animator.SetBool("Jump", false);
        }

        //Te lleva a la funcion Jump() con W y flecha

        if ((Input.GetKeyDown(KeyCode.W) || Input.GetKeyDown(KeyCode.UpArrow)) && Grounded)
        {
            if(Animator.GetBool("Slide") == false){
                Jump();
            }
            
        }
        if ((Input.GetKeyDown(KeyCode.S) || Input.GetKeyDown(KeyCode.DownArrow)) && Grounded)
        {
            Animator.SetBool("Slide", true);
            capsuleCollider2D.size = new Vector3(tamañoOriginal.x, tamañoOriginal.y * 0.5f, tamañoOriginal.z);
        }
        if((Input.GetKeyUp(KeyCode.S) || Input.GetKeyUp(KeyCode.DownArrow)) && Grounded)
        {
            Animator.SetBool("Slide", false);
            capsuleCollider2D.size = tamañoOriginal;
        }

        //Cuando pulsas S o flecha hacia abajo te lleva a la funcion fall

        if (Input.GetKeyDown(KeyCode.S) || Input.GetKeyDown(KeyCode.DownArrow))
        {
            Fall();
        }
    }

    //Funcion para que salte el personaje
    private void Jump()
    {
        Camera.main.GetComponent<AudioSource>().PlayOneShot(jump);
        Rigidbody2D.AddForce(Vector2.up * JumpForce);
        
    }
    //Funcion para que el personaje baje cuando esta en el aire por si quieres que baje mas rapido
    private void Fall()
    {
        Rigidbody2D.AddForce(Vector2.down * JumpForce);
    }
    //Funcion para que el personaje pierda cuando choca con un objeto
    private void OnTriggerEnter2D(Collider2D other)
    {
        if(other.CompareTag("Obstacle")){
            RunnerGameManager.Instance.GameOver();
        }
    }
}
