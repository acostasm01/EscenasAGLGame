using System.Text;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Buttons : MonoBehaviour
{
    public GameObject ERROR;
    private GameStateManager manager;
    private void Start()
    {
        manager = GameStateManager.instance;
    }
    public void salir(TMP_Text name)
    {
        if (name.text.Length == 1)
        {
            ERROR.SetActive(true);
            return;
        }
        StringBuilder sb = new StringBuilder();
        sb.Append(name.text + ";" + Giro.getClicks() + ";" + GameObject.Find("TimeTMP").GetComponent<TMP_Text>().text);
        if (Guardado.guardarpuntos(sb.ToString()))
        {
            manager.MarcarEscenaComoCompletada(SceneManager.GetActiveScene().buildIndex);
        }
    }
    public void reiniciar(TMP_Text name)
    {
        //Puntos punto = new Puntos(Giro.getClicks(), GameObject.Find("TimeTMP").GetComponent<TMP_Text>().text); 
        if (name.text.Length == 1)
        {
            ERROR.SetActive(true);
            return;
        }
        StringBuilder sb = new StringBuilder();
        sb.Append(name.text + ";" + Giro.getClicks() + ";" + GameObject.Find("TimeTMP").GetComponent<TMP_Text>().text);
        Guardado.guardarpuntos(sb.ToString());
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        ERROR.SetActive(false);
    }

    public void jugar(GameObject canvas)
    {
        Time.timeScale = 1.0f;
        canvas.SetActive(false);    
        Giro.fin = false;
    }

}
