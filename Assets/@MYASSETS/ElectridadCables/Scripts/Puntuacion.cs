using TMPro;
using UnityEngine;

public class Puntuacion : MonoBehaviour
{
    public TMP_Text tiempo,
        clicks_Jugador,
        tiempo_Jugador,
        nombres_lista,
        clicks_lista,
        tiempo_lista;
    private bool primero;
    private void Start()
    {
        primero = true;
        var puntos = Guardado.leerPuntos();
        int fin = 0;
        foreach (var p in puntos)
        {
            if (fin == 7)
            {
                break;
            }

            string[] subString = p.Split(';');
            if (primero)
            {
                nombres_lista.text = subString[0];
                clicks_lista.text = subString[1];
                tiempo_lista.text = subString[2];
                primero = false;
            }
            else
            {
                nombres_lista.text = nombres_lista.text + System.Environment.NewLine + subString[0];
                clicks_lista.text = clicks_lista.text + System.Environment.NewLine + subString[1];
                tiempo_lista.text = tiempo_lista.text + System.Environment.NewLine + subString[2];
            }

            fin++;
        }
    }

    public void playerPoints()
    {
        clicks_Jugador.text = Giro.getClicks().ToString();
        tiempo_Jugador.text = tiempo.text;
    }
}
