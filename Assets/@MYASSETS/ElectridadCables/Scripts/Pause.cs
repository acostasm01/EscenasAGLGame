using UnityEngine;
using UnityEngine.SceneManagement;

public class Pause : MonoBehaviour
{
    public GameObject UIPausa, pausaBTN;
    public bool esc = true;
    private GameStateManager manager;

    private void Start()
    {
        manager = GameStateManager.instance;
    }
    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (esc)
            {
                pausaBTN.SetActive(false);
                Giro.fin = true;
                UIPausa.SetActive(true);
                Time.timeScale = 0;
                esc = false;
            }
            else
            {
                pausaBTN.SetActive(true);
                UIPausa.SetActive(false);
                Giro.fin = false;
                Time.timeScale = 1;
                esc = true;
            }
        }
    }

    public void salir()
    {
        manager.QuitToMainScene();
    }

    public void resume()
    {
        pausaBTN.SetActive(true);
        UIPausa.SetActive(false);
        Giro.fin = false;
        Time.timeScale = 1;
    }

    public void reiniciar()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    public void pausa()
    {
        if (esc)
        {
            pausaBTN.SetActive(false);
            Giro.fin = true;
            UIPausa.SetActive(true);
            Time.timeScale = 0;
            esc = false;
        }
        else
        {
            pausaBTN.SetActive(true);
            UIPausa.SetActive(false);
            Giro.fin = false;
            Time.timeScale = 1;
            esc = true;
        }
    }
}
