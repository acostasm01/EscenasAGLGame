using System.Collections;
using TMPro;
using UnityEngine;

public class timer : MonoBehaviour
{
    public int minutes,
        seconds;
    public GameObject Fin, hasPerdido;
    private string second;
    public TMP_Text Timer;
    bool espera;
    // Start is called before the first frame update
    void Start()
    {
        second = seconds.ToString();
        if (seconds >= 0 && seconds < 10)
            second = "0" + seconds.ToString();
        StartCoroutine(cuentaAtras());
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (minutes == 0 & seconds == 0)
        {
            StopAllCoroutines();
            hasPerdido.SetActive(true);
            Fin.SetActive(true);
        }
        Timer.text = minutes.ToString() + ":" + second;
        if (espera)
        {
            StartCoroutine(cuentaAtras());
        }


    }

    IEnumerator cuentaAtras()
    {
        if (minutes != 0 || seconds != 0)
        {
            string sec = "";
            espera = false;
            yield return new WaitForSecondsRealtime(1f);
            if (seconds > 0)
            {
                seconds--;
                sec = seconds.ToString();
                if (seconds >= 0 && seconds < 10)
                    sec = "0" + sec;
            }
            else if (seconds == 0)
            {
                minutes--;
                seconds = 59;
                sec = seconds.ToString();
            }
            second = sec;
            espera = true;
        }
    }

}
