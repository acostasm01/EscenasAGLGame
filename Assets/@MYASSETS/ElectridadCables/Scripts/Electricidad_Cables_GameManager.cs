using System.Collections.Generic;
using UnityEngine;

public class Electricidad_Cables_GameManager : MonoBehaviour
{
    public AudioSource audio_rojo,
        audio_azul,
        audio_tierra;

    public SpriteRenderer fin_rojo,
        fin_azul,
        fin_tierra;

    public Sprite sprite_fin_rojo,
        sprite_fin_azul,
        sprite_fin_tierra;

    public GameObject UIfIn, hasGanado;
    public List<GameObject> Caminos;


    private Sprite sprite_inicio_rojo,
        sprite_inicio_azul,
        sprite_inicio_tierra;

    private int cables_utiles_rojos,
        cables_utiles_azules,
        cables_utiles_tierra;

    private List<string> cables_correctos_rojos = new List<string>(),
        cables_correctos_azules = new List<string>(),
        cables_correctos_tierra = new List<string>();

    private List<GameObject> cables_rojos = new List<GameObject>(),
        cables_azules = new List<GameObject>(),
        cables_tierra = new List<GameObject>();

    private bool audio_rojo_bien = true,
        audio_azul_bien = true,
        audio_tierra_bien = true;

    private void Start()
    {
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
        Guardado.leerPuntos();
        var game = GameObject.Find("Game");

        Debug.Log(Caminos.Count);

        for (int i = 0; i < game.transform.childCount; i++)
        {
            var cable = game.transform.GetChild(i).gameObject;
            if (cable.name.Contains("Inicio") || cable.name.Contains("Fin")) break;
            //Debug.Log(game.transform.GetChild(i).name);

            if (cable.GetComponent<Giro>().getPosicion_correcta().Length != 0)
            {
                var color = cable.GetComponent<Giro>().GetColor().ToString();
                if (color.Equals("rojo"))
                    cables_rojos.Add(cable);
                else if (color.Equals("azul"))
                    cables_azules.Add(cable);
                else if (color.Equals("tierra"))
                    cables_tierra.Add(cable);
            }
        }

        cables_rojos.TrimExcess();
        cables_azules.TrimExcess();
        cables_tierra.TrimExcess();

        cables_utiles_rojos = cables_rojos.Count;
        cables_utiles_azules = cables_azules.Count;
        cables_utiles_tierra = cables_tierra.Count;

        sprite_inicio_rojo = fin_rojo.sprite;
        sprite_inicio_azul = fin_azul.sprite;
        sprite_inicio_tierra = fin_tierra.sprite;
    }

    void Update()
    {
        if (cables_utiles_rojos == cables_correctos_rojos.Count && cables_utiles_azules == cables_correctos_azules.Count && cables_utiles_tierra == cables_correctos_tierra.Count)
        {
            Giro.fin = true;
            Time.timeScale = 0;
            GetComponent<Puntuacion>().playerPoints();
            hasGanado.SetActive(true);
            UIfIn.SetActive(true);
        }

        audio_rojo_bien = Comprobar_camino(cables_utiles_rojos, cables_correctos_rojos.Count, fin_rojo, sprite_fin_rojo, sprite_inicio_rojo, audio_rojo, audio_rojo_bien);
        audio_azul_bien = Comprobar_camino(cables_utiles_azules, cables_correctos_azules.Count, fin_azul, sprite_fin_azul, sprite_inicio_azul, audio_azul, audio_azul_bien);
        audio_tierra_bien = Comprobar_camino(cables_utiles_tierra, cables_correctos_tierra.Count, fin_tierra, sprite_fin_tierra, sprite_inicio_tierra, audio_tierra, audio_tierra_bien);

        Comprobar(cables_rojos, cables_correctos_rojos);
        Comprobar(cables_azules, cables_correctos_azules);
        Comprobar(cables_tierra, cables_correctos_tierra);

    }

    private void Comprobar(List<GameObject> list_cables, List<string> cables_comprobados)
    {
        foreach (GameObject cable in list_cables)
            foreach (float posicion in cable.GetComponent<Giro>().getPosicion_correcta())
            {
                //Debug.Log("Object angle: " + cable.transform.rotation.eulerAngles.z + " position angle: " + posicion);
                var angleZ = Mathf.RoundToInt(cable.transform.rotation.eulerAngles.z);
                if (angleZ == posicion && cable.GetComponent<Giro>().IsConnected() == false)
                {
                    if (!cables_comprobados.Contains(cable.name))
                        cables_comprobados.Add(cable.name);
                    break;
                }
                if (angleZ != posicion)
                    if (cables_comprobados.Contains(cable.name))
                        cables_comprobados.Remove(cable.name);
            }
    }

    private bool Comprobar_camino(int cables_utiles, int cables_correctos, SpriteRenderer object_sprite, Sprite sprite_correcto, Sprite sprite_incorrecto, AudioSource audio_source, bool audio)
    {
        if (cables_utiles == cables_correctos)
        {
            if (audio) { audio_source.Play(); }
            object_sprite.sprite = sprite_correcto;
            return false;
        }
        else
        {
            object_sprite.sprite = sprite_incorrecto;
            return true;
        }

    }
}
