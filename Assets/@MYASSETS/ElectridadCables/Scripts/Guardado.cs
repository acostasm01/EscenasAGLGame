using Newtonsoft.Json;
using System.Collections.Generic;
using UnityEngine;

public class PointComparer : IComparer<string>
{
    public int Compare(string x, string y)
    {
        string[] splitx = x.Split(";");
        string[] splity = y.Split(";");
        int ix = int.Parse(splitx[1]), iy = int.Parse(splity[1]);
        if (ix > iy)
        {
            return 1;
        }
        else if (ix < iy)
        {
            return -1;
        }
        return 0;
    }
}
public class Guardado
{
    private static List<string> puntos = new List<string>();
    public static List<string> GetPuntos() { return puntos; }

    public static List<string> leerPuntos()
    {
        if (PlayerPrefs.HasKey("Puntos"))
        {
            puntos = JsonConvert.DeserializeObject<List<string>>(PlayerPrefs.GetString("Puntos"));
        }
        return puntos;
    }

    public static bool guardarpuntos(string punto)
    {
        puntos.Add(punto);
        puntos.Sort(new PointComparer());
        PlayerPrefs.SetString("Puntos", JsonConvert.SerializeObject(puntos, Formatting.Indented));
        return true;
    }
}
