using TMPro;
using UnityEngine;

public enum color
{
    rojo, azul, tierra
}

public class Giro : MonoBehaviour
{
    public static bool fin = false;
    private static int clicks;

    public float[] posicion_correcta;
    public color color;

    private bool connected = false;
    private int[] posicionesRND = { 0, 90, 180, 270 };

    public static int getClicks() { return clicks; }
    public void SetConnected(bool connected) { this.connected = connected; }
    public bool IsConnected() { return connected; }
    public color GetColor() { return color; }

    public float[] getPosicion_correcta() { return posicion_correcta; }
    private void Start()
    {
        clicks = 0;
        var z = posicionesRND[Random.Range(0, posicionesRND.Length)];
        this.transform.Rotate(new Vector3(0, 0, z));
    }
    private void OnMouseUp()
    {
        if (!fin)
        {
            GetComponent<AudioSource>().Play();
            this.transform.Rotate(new Vector3(0, 0, 90f));
            clicks++;
            var text = GameObject.Find("Clicks");
            text.GetComponent<TMP_Text>().text = clicks.ToString();
        }
    }
}
